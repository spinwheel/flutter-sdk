#!/bin/bash

echo DELETE PUBSPEC.LOCK
rm pubspec.lock
echo FLUTTER CLEAN !SDK
flutter clean
echo FLUTTER PUB UPGRADE !SDK
flutter pub upgrade
echo FLUTTER PUB GET !SDK
flutter pub get
echo BYE!
exit
