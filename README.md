# README

First time here? Maybe check the [overview doc](https://docs.google.com/presentation/d/19c4SCVb4fjG9x3m4LhsgJkybEF3m7sDI7pmNU7zquo4/edit?usp=sharing) and our [test app](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/lib/test_app/) first !

If looking for usage on our packages, the [usage docs](https://bitbucket.org/spinwheel/flutter-sdk/src/master/doc/usage.md) contains the setup instructions.

Our [change log](https://bitbucket.org/spinwheel/flutter-sdk/src/master/doc/change_log.md) and [release notes](https://bitbucket.org/spinwheel/flutter-sdk/src/master/doc/release_notes.md) mentions the latest changes.

If you want to better understand how our long-term support for styling, layouts and customization options work, please refer to [LTS styling](https://bitbucket.org/spinwheel/flutter-sdk/src/master/doc/lts_styling.md), [LTS layouts](https://bitbucket.org/spinwheel/flutter-sdk/src/master/doc/lts_layout.md) and [LTS customization](https://bitbucket.org/spinwheel/flutter-sdk/src/master/doc/lts_customization.md).

For details on publishing options, we go into some details on our [publishing doc](https://bitbucket.org/spinwheel/flutter-sdk/src/master/doc/publishing.md)

We are currently working on our [analytics](https://bitbucket.org/spinwheel/flutter-sdk/src/master/doc/analytics.md) implementation, so the corresponding docs are also WIP.

