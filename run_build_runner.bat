@echo off

cd sw_core
call flutter pub run build_runner build
cd ..
cd sw_auth
call flutter pub run build_runner build
cd ..
cd sw_data
call flutter pub run build_runner build
cd ..
cd sw_component
call flutter pub run build_runner build
cd ..
cd sw_dropin
call flutter pub run build_runner build
cd ..
call flutter pub run build_runner build
PAUSE