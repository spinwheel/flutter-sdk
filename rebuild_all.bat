@echo off

cd "..\flutter_core\"
call run_rebuild.bat
cd "..\flutter_component\"
call run_rebuild.bat
cd "..\flutter_data\"
call run_rebuild.bat
cd "..\flutter_auth\"
call run_rebuild.bat
cd "..\flutter_loan_connect\"
call run_rebuild.bat
cd "..\flutter_sdk\"
call run_rebuild.bat