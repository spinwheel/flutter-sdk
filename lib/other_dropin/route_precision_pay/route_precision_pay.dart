import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/box_card.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/outlined_button.dart';
import 'package:sw_component/components/widget/precision_pay_card.dart';
import 'package:sw_component/components/widget/standard_button.dart';
import 'package:sw_component/components/widget/sw_line_chart.dart';
import 'package:sw_component/components/widget/sw_slider.dart';
import 'package:sw_component/components/widget/sw_toggle_button.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_core/tool/state.dart';

class ParamsPrecisionPay {
  String name;
  String transactionHistoryPath;
  String loanListNavigationText;
  String extraPayNavigationText;
  String roundUpNavigationText;
  String oneTimePayNavigationText;
  String collaboratorNavigationPath;
  String collaboratorNavigationText;

  bool hideTransactionHistoryNavigation;
  bool hideLoanListNavigation;
  bool hideExtraPayNavigation;
  bool hideRoundUpNavigation;
  bool showOneTimePayNavigation;
  bool hideCollaboratorNavigation;

  ParamsPrecisionPay({
    this.name = "",
    this.transactionHistoryPath = "",
    this.loanListNavigationText = "",
    this.extraPayNavigationText = "Extra Pay",
    this.roundUpNavigationText = "Round-Ups",
    this.oneTimePayNavigationText = "Make Payment",
    this.collaboratorNavigationPath = "",
    this.collaboratorNavigationText = "Community Pay",
    this.hideTransactionHistoryNavigation = false,
    this.hideLoanListNavigation = false,
    this.hideExtraPayNavigation = false,
    this.hideRoundUpNavigation = false,
    this.showOneTimePayNavigation = false,
    this.hideCollaboratorNavigation = false,
  });
}

class RoutePrecisionPay extends StatelessWidget {
  RoutePrecisionPay({
    Key? key,
    required this.params,
    this.themeDark,
    this.themeLight,
  }) : super(key: key);

  final ParamsPrecisionPay params;
  final ThemeData? themeDark;
  final ThemeData? themeLight;

  final _isSelected = [true, false].obs;
  final _value = 0.0.obs;

  @override
  Widget build(BuildContext context) => MaterialThemeWrapper(
        child: Scaffold(
          backgroundColor: Color(0xffF6F6FA),
          appBar: AppBar(
            shadowColor: Colors.transparent,
            backgroundColor: Colors.transparent,
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              color: Colors.purple,
              onPressed: () => Navigator.pop(context),
            ),
          ),
          body: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(17),
              child: Flex(
                direction: Axis.vertical,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  _monthlyPaymentCard(context),
                  SizedBox(
                    height: 73,
                  ),
                  _loanImpact(),
                  _lineChart(),
                  SizedBox(
                    height: 24,
                  ),
                  _footerCard(context),
                  SizedBox(
                    height: 29,
                  ),
                  StandardButton("Pay now", () {}, height: 48),
                ],
              ),
            ),
          ),
        ),
      );

  _monthlyPaymentCard(BuildContext context) => Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 44,
            ),
            Text(
              "Extra monthly payment",
              style: TextStyle(
                fontSize: 14,
              ),
            ),
            SizedBox(height: 32),
            Row(
              textBaseline: TextBaseline.alphabetic,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _toggleButton(),
                SizedBox(
                  width: 135,
                  child: TextFormField(),
                ),
              ],
            ),
            SizedBox(
              height: 26,
            ),
            _slider(context),
          ],
        ),
      );

  _cardHeader(BuildContext context) => BoxCard(
        margin: EdgeInsets.zero,
        height: 120,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(color: colorScheme.primary),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Suggested amount",
                  style: TextStyle(fontSize: 24),
                ),
                Text(
                  "10%",
                  style: TextStyle(fontSize: 32),
                ),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              "How much money this saves \$450",
              style: TextStyle(fontSize: 16),
            )
          ],
        ),
      );

  _lineChart() => BoxCard(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.all(2),
        height: 150,
        child: Center(
          child: SWLineChart(
            y: 25.4,
            x: 0.3,
          ),
        ),
      );

  _slider(BuildContext context) => Observer(
        () => SWSlider(_value.value, 20, 0, (integer) {
          _value.value = integer;
        }, MediaQuery.of(context).size.width, "assets/images/slider_logo.png", "Range"),
      );

  _toggleButton() => Observer(
        () => SWToggleButton(
          isSelected: _isSelected.value,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 12),
              child: Text("Amount", style: TextStyle(fontSize: 14)),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 12),
              child: Text("Month", style: TextStyle(fontSize: 14)),
            ),
          ],
          onPressed: (int newIndex) {
            if (newIndex == 1) {
              _isSelected.value[0] = false;
              _isSelected.value[newIndex] = true;
              _isSelected.refresh();
            } else {
              _isSelected.value[newIndex] = true;
              _isSelected.value[1] = false;
              _isSelected.refresh();
            }
          },
        ),
      );

  _loanImpact() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Loan Impact",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 24,
          ),
          PrecisionPayCard(
            cardTitle: "Total Amount Pay",
            margin: EdgeInsets.zero,
            padding: EdgeInsets.all(12),
            chartValue: 7,
            currentValue: "67,409",
            wSpinwheelValue: "62,886",
            savingsValue: "4,523",
          ),
          SizedBox(
            height: 16,
          ),
          PrecisionPayCard(
            cardTitle: "Total Amount Pay",
            margin: EdgeInsets.zero,
            padding: EdgeInsets.all(12),
            chartValue: 13,
            currentValue: "67,409",
            wSpinwheelValue: "62,886",
            savingsValue: "4,523",
          ),
          SizedBox(
            height: 16,
          ),
        ],
      );

  _footerCard(BuildContext context) => BoxCard(
        margin: EdgeInsets.zero,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * .8,
              height: MediaQuery.of(context).size.height * .24,
              child: ImageCompat(
                'balcony.png',
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: Row(
                children: [
                  Text(
                    "CONNECT YOUR BANK ACCOUNT ",
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                  ImageCompat(
                    'info.svg',
                    width: 16,
                    height: 16,
                  )
                ],
              ),
            ),
            Text(
              "Required to make Payments",
              style: TextStyle(
                fontSize: 13,
              ),
            ),
            SizedBox(
              height: 26,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Text("Get out of debt faster and save money by connecting your bank account."),
                  SizedBox(
                    height: 30,
                  ),
                  _iconTexts(),
                  SizedBox(
                    height: 32,
                  ),
                  StandardButton("Connect bank account", () {}, height: 48),
                  SizedBox(
                    height: 16,
                  ),
                  SWOutlinedButton("Details & Options", () {}, height: 48)
                ],
              ),
            ),
          ],
        ),
      );

  _iconTexts() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _iconTextPayments(),
          const SizedBox(height: 18),
          _iconTextRoundUp(),
          const SizedBox(height: 16),
          _iconTextPayDown()
        ],
      );

  _iconTextPayments() => Row(
        crossAxisAlignment: CrossAxisAlignment.baseline,
        textBaseline: TextBaseline.alphabetic,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 4.0),
            child: ImageCompat(
              'Figure.svg',
              package: 'sw_loan_connect',
              width: 16,
              height: 16,
              fit: BoxFit.fitWidth,
              color: theme.primaryColor,
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 10),
            child: const Text(
              'Save money with extra payments',
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 13),
            ),
          ),
        ],
      );

  _iconTextRoundUp() => Row(
        crossAxisAlignment: CrossAxisAlignment.baseline,
        textBaseline: TextBaseline.alphabetic,
        children: [
          Flexible(
              child: Container(
            margin: EdgeInsets.only(right: 10, left: 4),
            child: ImageCompat(
              'arrows.svg',
              package: 'sw_loan_connect',
              height: 16,
              fit: BoxFit.fitWidth,
              color: theme.primaryColor,
            ),
          )),
          const Text(
            'Use round-up to pay loan faster',
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 13),
          )
        ],
      );

  _iconTextPayDown() => Row(
        crossAxisAlignment: CrossAxisAlignment.baseline,
        textBaseline: TextBaseline.alphabetic,
        children: [
          Container(
            margin: EdgeInsets.only(right: 10),
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                ImageCompat(
                  'bookmark.svg',
                  package: 'sw_loan_connect',
                  width: 9,
                  height: 9,
                  fit: BoxFit.fitWidth,
                  color: theme.primaryColor,
                ),
                ImageCompat(
                  'arrow_percent.svg',
                  package: 'sw_loan_connect',
                  width: 16,
                  height: 16,
                  fit: BoxFit.fitWidth,
                  color: theme.primaryColorDark,
                ),
              ],
            ),
          ),
          Flexible(
            child: Text(
              'Pay down the most expensive loan first',
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 13),
            ),
          ),
        ],
      );
}
