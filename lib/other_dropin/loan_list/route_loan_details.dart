import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/slim_appbar.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/entity_user/loans.dart';
import 'package:sw_core/tool/state.dart';

class RouteLoanDetails extends StatelessWidget {
  RouteLoanDetails(this.loan, this.servicer, {Key? key}) : super(key: key);

  final Loans loan;
  final Servicer servicer;

  @override
  Widget build(BuildContext context) => MaterialThemeWrapper(
          child: Scaffold(
        appBar: SlimAppBar('Loan Info'),
        body: _buildBody(),
      ));

  _buildBody() => Column(
        children: [
          _innerColumn(),
        ],
      );

  _innerColumn() => Expanded(
        child: SingleChildScrollView(
          child: Flex(
            mainAxisSize: MainAxisSize.min,
            direction: Axis.vertical,
            children: [
              _logo(),
              _details(),
              _percentageCard(),
              Flexible(
                child: SizedBox(
                  height: 80,
                ),
              ),
              _footer(),
            ],
          ),
        ),
      );

  _logo() => Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(32),
        child: ImageCompat(
          servicer.logoUrl,
          width: 192,
          height: 192,
        ),
      );

  _details() => Padding(
        padding: const EdgeInsets.fromLTRB(45, 45, 45, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(height: 8),
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Interest Rate',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      '${loan.interestRate.toString()}%',
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 32),
                    const Text(
                      'Status',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 16),
                    SizedBox(
                      width: 150,
                      child: Text(
                        loan.status ?? "",
                        style: const TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                    const SizedBox(height: 32),
                    const Text(
                      'Principal Balance',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      currency(loan.principalBalance, 0),
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 32),
                    const Text(
                      'Loan Type',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      formatFirstCharCapital(loan.loanType ?? ""),
                      style: const TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                const SizedBox(width: 20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Original Amount',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      currency(loan.origAmount, 0),
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 32),
                    const Text(
                      'Minimum Payment',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      currency(loan.regularMonthlyPayment),
                      style: const TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 32),
                    const Text(
                      'Outstanding Interest',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      "\$${loan.outstandingInterest.toString()}",
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 32),
                    const Text(
                      'Repayment Plan',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(height: 16),
                    SizedBox(
                      width: 160,
                      child: Text(
                        loan.repaymentPlan.toString(),
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 32),
          ],
        ),
      );

  _percentageCard() => Container(
        alignment: AlignmentDirectional.topStart,
        padding: const EdgeInsets.all(16),
        margin: const EdgeInsets.fromLTRB(30, 0, 30, 30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          shape: BoxShape.rectangle,
          border: Border.all(
            color: Colors.black,
            width: 0.5,
            style: BorderStyle.solid,
          ),
          color: colorScheme.surface,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              children: [
                SizedBox(
                  width: 16,
                ),
                Text(
                  'Debt-Free',
                  style: TextStyle(fontSize: 14),
                ),
                SizedBox(width: 62),
                Text(
                  'Estimated ',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  formatUTC(loan.expectedPayoffDate ?? ''),
                  style: TextStyle(fontSize: 14),
                ),
              ],
            ),
            const SizedBox(height: 24),
            LinearPercentIndicator(
              curve: Curves.easeInOut,
              animation: true,
              animationDuration: 2000,
              lineHeight: 8.0,
              percent: _calcPaidOff(),
              progressColor: colorScheme.primary,
            ),
            const SizedBox(height: 24),
            Text('${(_calcPaidOff() * 100).toStringAsFixed(2)}% Paid-Off',
                style: const TextStyle(fontSize: 12)),
          ],
        ),
      );

  _footer() => Container(
        margin: EdgeInsets.only(left: 10, right: 10, bottom: 9),
        height: 64,
        width: double.infinity,
        child: ElevatedButton(
          onPressed: () {},
          child: const Text(
            'Configure Payment',
            style: TextStyle(fontSize: 16),
          ),
        ),
      );

  _calcPaidOff() {
    double toPayPercent = (loan.principalBalance!.toDouble() / loan.origAmount!.toDouble());
    return toPayPercent >= 1 ? toPayPercent - 1 : toPayPercent;
  }
}
