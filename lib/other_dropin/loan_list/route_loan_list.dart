import 'package:flutter/material.dart';
import 'package:flutter_sw/other_dropin/loan_list/route_loan_details.dart';
import 'package:sw_component/components/widget/card_button.dart';
import 'package:sw_component/components/widget/data_card.dart';
import 'package:sw_component/components/widget/fetcher.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/sliver_scroll.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/entity_user/loans.dart';
import 'package:sw_core/entity_user/user.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/navigation/nav.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_core/tool/state.dart';
import 'package:sw_data/usecase/use_case_servicer.dart';

class ParamsLoanList {
  ParamsLoanList({
    this.isNested = true,
  });

  bool isNested;
}

class RouteLoanList extends StatelessWidget {
  RouteLoanList({Key? key}) : super(key: key);

  final ParamsLoanList params = put<ParamsLoanList>(() => ParamsLoanList(isNested: true));
  final UseCaseServicer usecaseServicer = get<UseCaseServicer>();
  final IUseCaseAuth controllerAuth = get<IUseCaseAuth>();

  User get user => controllerAuth.user.value;
  BuildContext? context;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return MaterialThemeWrapper(
      child: Scaffold(
        body: _body(),
      ),
    );
  }

  _body() => Center(child: _caseUserSuccess());

  Widget _caseUserSuccess() {
    return params.isNested ? _nestedContent() : _content();
  }

  _content() => SliverScroll(
        title: 'Loans',
        children: [
          _scrollable(),
        ],
      );

  _nestedContent() => Center(
        child: SingleChildScrollView(child: _scrollable()),
      );

  _scrollable() => Padding(
        padding: const EdgeInsets.fromLTRB(15, 0, 15, 30),
        child: Observer(
          () => Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ..._loans(),
              const SizedBox(height: 32),
              ..._footerButtons(),
            ],
          ),
        ),
      );

  List<Widget> _loans() {
    List<Widget> list = [];

    for (var sla in user.studentLoanAccounts) {
      Servicer? servicer;
      String servicerID = sla.loanServicerId ?? "";
      if (servicerID.isNotEmpty) {
        list.add(Fetcher<Servicer>(
          fetch: () => usecaseServicer.execute(servicerID),
          onSuccess: (serv) {
            servicer = serv;
            return Column(
              children: [
                const SizedBox(height: 16),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    servicer?.name ?? "",
                    style: const TextStyle(fontSize: 26),
                  ),
                ),
                const SizedBox(height: 16),
              ],
            );
          },
        ));
      }

      for (var acc in sla.loanAccounts) {
        for (Loans loan in acc.loans!) {
          list.add(
            DataCard(
              value: currency(loan.origAmount, 0).replaceAll('\$', ''),
              due: formatUTC(loan.dueDate),
              rate: loan.interestRate.toString(),
              status: loan.status ?? "",
              onPressed: () => navigate(
                context,
                RouteLoanDetails(loan, servicer ?? Servicer()),
              ),
              decoration: _dataCardDecor(),
            ),
          );
        }
      }
    }

    return list;
  }

  _dataCardDecor() => BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        border: Border.all(
          color: Color(0xffEFEFF6),
          width: 1.5,
          style: BorderStyle.solid,
        ),
        color: colorScheme.surface,
      );

  _footerButtons() => const [
        CardButton(
          'Spare Change Round Up',
          'Save 2 years & 5k* - Put that spare change to work',
        ),
        CardButton(
          'Extra Pay',
          'Save 4 years & 10k* - A few extra dollars each payment makes a BIG difference',
        )
      ];
}
