/// Test data file for development purposes only. For QA and validation, replace
/// below values with your own test cases

/// Replace [localHost] with your own host implementation
const localHost = 'http://localhost:8081/';

/// Replace [remoteHost] with your own host implementation
const remoteHost = 'https://swsl-backend.herokuapp.com/';
const remoteHostSandBox = 'https://yuri-swsl-test.herokuapp.com/';

const extUserID = 'testuser@gmail.com';
const multiStepServicer = 'f0f3de8c-9a9c-4d0c-8416-e71a97b81ebc';
const singleStepServicer = '77a549e1-d67d-45f2-94a4-e3ef7041af92';
const userID = '72442002-2ad5-4711-8910-091ef3efb6c6';
const testUser = 'testUser';
const testPass = 'testPass';