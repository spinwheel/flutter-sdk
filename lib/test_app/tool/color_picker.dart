import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:get/get_rx/get_rx.dart';
import 'package:injector/injector.dart';
import 'package:sw_core/tool/observer.dart';

class ColorSelectorDialog extends StatelessWidget {
  ColorSelectorDialog({required Rx<Color> color, Key? key})
      : color = color,
        super(key: key);

  final color;

  final _injector = Injector.appInstance;

  Widget build(BuildContext context) => colorPicker(context);

  colorPicker(BuildContext context) => GestureDetector(
        onTap: () async {
          await _showColorPicker(context);
          _disposeFocus(context);
        },
        child: _colorPickerButton(),
      );

  _disposeFocus(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  _colorPickerButton() => Observer(
        () => Stack(
          alignment: Alignment.center,
          children: [
            Container(
              color: color.value,
            ),
            _colorPickerLabel()
          ],
        ),
      );

  _colorPickerLabel() => Observer(
        () => Text(
          "Pick the color here!",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: _checkLuminance(),
          ),
        ),
      );

  _checkLuminance() => color.value.computeLuminance() <= 0.5
      ? color.value.withRed(255).withBlue(255).withGreen(255)
      : color.value.withRed(0).withBlue(0).withGreen(0);

  _showColorPicker(BuildContext context) => showDialog(
        context: context,
        builder: (context) => Dialog(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Observer(
                  () => ColorPicker(
                    pickerColor: color.value,
                    onColorChanged: (colorChanged) {
                      color.value = colorChanged;
                      _resetColor();
                    },
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                  child: Text("Done"),
                )
              ],
            ),
          ),
        ),
      );

  _resetColor() {
    if (_injector.exists<Color>()) {
      _injector.removeByKey<Color>();
    }
    _injector.registerSingleton<Color>(() => color.value);
  }
}
