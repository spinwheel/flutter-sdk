import 'dart:ui';

import 'package:flutter/material.dart';

navigateTo(BuildContext context, Widget page) =>
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => page));

navigateAsWidget(BuildContext context, Widget page, {String? title, Function()? onBackPressed}) =>
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => _scaffoldAsWidget(page, context,
            onBackPressed: () => onBackPressed != null ? onBackPressed() : Navigator.pop(context),
            title: title ?? "Navigating as widget"),
      ),
    );

_scaffoldAsWidget(
  Widget body,
  BuildContext context, {
  List<Widget>? appBarActions,
  Function()? onBackPressed,
  String? title,
}) =>
    Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_outlined,
          ),
          onPressed: onBackPressed ?? () => Navigator.pop(context),
        ),
        actions: appBarActions,
        title: Text(title ?? "Navigating as a Widget"),
      ),
      body: Container(
        margin: EdgeInsets.only(top: kToolbarHeight),
        height: window.physicalSize.height,
        child: body,
      ),
    );
