import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:sw_core/navigation/nav.dart';

Future<Widget> loadWidget(Future<Widget> future) async => FutureBuilder<Widget>(
      future: future,
      builder: (context, snapshot) => snapshot.hasData ? snapshot.data! : _loading(),
    );

Widget markdownScaffold(Widget markdown, [String? title]) => Scaffold(
      appBar: AppBar(
        title: Text(
          title ?? "",
        ),
      ),
      body: markdown,
    );

Widget styledMarkdown(String markdown) => Markdown(
      data: markdown,
      padding: const EdgeInsets.all(20),
      onTapLink: (text, href, title) => launchURL(href!),
    );

Widget _loading() => const Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
      ),
    );
