import 'package:flutter/services.dart';

const String assetDocs = "doc";

Future<String> loadMarkdown(String asset) async => await rootBundle.loadString(asset);
