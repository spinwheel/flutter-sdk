import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future initStorage() async => _prefs = await SharedPreferences.getInstance();

late final SharedPreferences _prefs;

Future saveBoolean(String key, bool value) async => await _prefs.setBool(key, value);
Future saveString(String key, String value) async => await _prefs.setString(key, value);
Future saveInt(String key, int value) async => await _prefs.setInt(key, value);
Future saveDouble(String key, double value) async => await _prefs.setDouble(key, value);

bool? getBoolean(String key) => _prefs.getBool(key);
String? getString(String key) => _prefs.getString(key);
int? getInt(String key) => _prefs.getInt(key);
double? getDouble(String key) => _prefs.getDouble(key);
List<String>? getStringList(String key) => _prefs.getStringList(key);

enum StorageLabel {
  logEnabled,
}

extension ExtStorageLabel on StorageLabel {
  String get name => describeEnum(this);
}
