import 'package:flutter/material.dart';
import 'package:flutter_sw/test_app/tool/storage.dart';
import 'package:flutter_sw/test_app/view/pages/test_app.dart';
import 'package:sw_core/tool/log.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initStorage();
  if (getBoolean(StorageLabel.logEnabled.name) == true) {
    SWLogger();
  }
  runApp(TestApp());
}

class TestApp extends MaterialApp {
  const TestApp({Key? key}) : super(key: key);

  @override
  Widget get home => TestHome();

  @override
  String get title => 'Test App';

  @override
  bool get debugShowCheckedModeBanner => true;

  @override
  ThemeMode get themeMode => ThemeMode.system;
}
