import 'package:flutter/material.dart';
import 'package:flutter_sw/test_app/tool/asset.dart';
import 'package:flutter_sw/test_app/tool/markdown.dart';

Future<Widget> changelog() async => markdownScaffold(
      styledMarkdown(
        await _loadChangelogMd(),
      ),
      "Changelog",
    );

Future<Widget> releaseNotes() async => markdownScaffold(
      styledMarkdown(
        await _loadReleaseNotesMd(),
      ),
      "Release Notes",
    );

Future<Widget> usage() async => markdownScaffold(
      styledMarkdown(
        await _loadUsageMd(),
      ),
      "Usage",
    );

Future<String> _loadChangelogMd() async => await loadMarkdown("$assetDocs/change_log.md");

Future<String> _loadReleaseNotesMd() async => await loadMarkdown("$assetDocs/release_notes.md");

Future<String> _loadUsageMd() async => await loadMarkdown("$assetDocs/usage.md");
