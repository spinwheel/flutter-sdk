import 'package:flutter/material.dart';
import 'package:flutter_sw/other_dropin/loan_list/route_loan_list.dart';
import 'package:flutter_sw/other_dropin/loan_pal/route_loan_pal.dart';
import 'package:flutter_sw/other_dropin/route_precision_pay/route_precision_pay.dart';
import 'package:flutter_sw/other_dropin/transaction_history/route_transaction_history.dart';
import 'package:flutter_sw/test_app/init/init_loan_connect.dart';
import 'package:flutter_sw/test_app/tool/nav.dart';
import 'package:sw_core/params/params_loan_pal.dart';

List<Widget> tiles(BuildContext context) => [
      _tileLoanConnect(context),
      _tileLoanList(context),
      _tileTransactionHistory(context),
      _tileLoanPal(context),
      _tilePrecisionPay(context),
    ];

_tilePrecisionPay(BuildContext context) => Container(
      height: 100,
      child: ListTile(
        onTap: () => navigateTo(context, RoutePrecisionPay(params: ParamsPrecisionPay())),
        title: _newTile(
          Icon(
            Icons.payments,
            size: 40,
            color: Colors.blue,
          ),
          "Precision pay",
          Icon(
            Icons.arrow_forward_ios,
            color: Colors.blue,
          ),
        ),
      ),
    );

_tileLoanPal(BuildContext context) => Container(
      height: 100,
      child: ListTile(
        onTap: () => navigateTo(
          context,
          RouteLoanPal(params: ParamsLoanPal()),
        ),
        title: _newTile(
          Icon(
            Icons.attach_money,
            size: 40,
            color: Colors.blue,
          ),
          "Loan Pal",
          Icon(
            Icons.arrow_forward_ios,
            color: Colors.blue,
          ),
        ),
      ),
    );

_tileTransactionHistory(BuildContext context) => Container(
      height: 100,
      child: ListTile(
        onTap: () => navigateTo(
          context,
          RouteTransactionHistory(
            params: ParamsTransactionHistory(),
          ),
        ),
        title: _newTile(
          Icon(
            Icons.history,
            size: 40,
            color: Colors.blue,
          ),
          "Transaction History",
          Icon(
            Icons.arrow_forward_ios,
            color: Colors.blue,
          ),
        ),
      ),
    );

_tileLoanList(BuildContext context) => Container(
      height: 100,
      child: ListTile(
        onTap: () => navigateTo(
          context,
          RouteLoanList(),
        ),
        title: _newTile(
          Icon(
            Icons.list,
            size: 40,
            color: Colors.blue,
          ),
          "Loan List",
          Icon(
            Icons.arrow_forward_ios,
            color: Colors.blue,
          ),
        ),
      ),
    );

_tileLoanConnect(BuildContext context) => Container(
      height: 100,
      child: ListTile(
        onTap: () async => navigateAsWidget(
          context,
          await init(paramsConnectDefault()),
          title: "Connect",
        ),
        title: _newTile(
          Icon(
            Icons.connect_without_contact,
            size: 40,
            color: Colors.blue,
          ),
          "Loan Connect",
          Icon(
            Icons.arrow_forward_ios,
            color: Colors.blue,
          ),
        ),
      ),
    );

_newTile(Widget icon, String text, Widget leading) => Row(
      children: [
        icon,
        Container(
          margin: EdgeInsets.only(left: 16),
          child: Text(text),
        ),
        Expanded(child: Container()),
        leading,
      ],
    );
