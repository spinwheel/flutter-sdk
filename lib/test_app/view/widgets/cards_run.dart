import 'package:flutter/material.dart';
import 'package:flutter_sw/assets/json_themes.dart';
import 'package:flutter_sw/test_app/init/init_custom_layout.dart';
import 'package:flutter_sw/test_app/init/init_loan_connect.dart';
import 'package:flutter_sw/test_app/init/init_showcase.dart';
import 'package:flutter_sw/test_app/tool/nav.dart';
import 'package:flutter_sw/test_app/view/pages/lume_test.dart';
import 'package:sw_component/components/CATE/card_run.dart';

List<Widget> cardsRun(
  BuildContext context, {
  required Widget Function(BuildContext) widgetsProvidedBuilder,
  Function()? onBackLandingPageProvided,
  Function()? onBackLayoutB,
  Function()? onBackLayoutDefault,
}) =>
    [
      _cardLumeTest(context),
      _cardLayoutDefault(context, onBackPressed: onBackLayoutDefault),
      _cardLayoutB(context, onBackPressed: onBackLayoutB),
      _cardLandingPageProvided(context, onBackPressed: onBackLandingPageProvided),
      _cardWidgetsProvided(context, widgetsProvidedBuilder),
      _cardCustomTextFields(context),
      _cardRoutingScenarios(context),
      _cardLayoutCustom(context),
    ];

_cardLumeTest(BuildContext context, {Function()? onBackPressed}) => CardRun(
      "Lume test",
      () => navigateTo(context, LumeTestApp()),
    );

_cardLayoutDefault(BuildContext context, {Function()? onBackPressed}) => CardRun(
      "Layout default",
      () async => navigateAsWidget(context, await init(paramsConnectDefault()),
          title: "Layout Default", onBackPressed: onBackPressed),
    );

_cardLayoutB(BuildContext context, {Function()? onBackPressed}) => CardRun(
      "Layout B",
      () async => navigateAsWidget(
        context,
        await init(paramsConnectB(), sampleTheme, sampleTheme),
        title: "Layout B",
        onBackPressed: onBackPressed,
      ),
    );

_cardLandingPageProvided(BuildContext context, {Function()? onBackPressed}) => CardRun(
      "Partner params with landing page provided",
      () async => navigateAsWidget(
        context,
        await init(paramsProvidedLandingPage()),
        title: "Partner params provided landing page",
        onBackPressed: onBackPressed,
      ),
    );

_cardWidgetsProvided(BuildContext context, Widget Function(BuildContext) builder) => CardRun(
      "Partner params with widgets provided",
      () async {
        await showDialog(
          context: context,
          builder: builder,
        );
      },
    );

_cardCustomTextFields(BuildContext context) => CardRun(
      "Layout custom text fields",
      () async => await showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: runConfigDialog(
              paramsConnectB(),
              context,
            ),
          );
        },
      ),
    );

_cardRoutingScenarios(BuildContext context) => CardRun(
      "Routing scenarios",
      () => navigateTo(
        context,
        _openScenarios(context),
      ),
    );

_cardLayoutCustom(BuildContext context) => CardRun(
      "Layout custom",
      () => navigateTo(context, CustomLayoutForm()),
    );

_openScenarios(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: Text("Scenarios"),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Flex(
          direction: Axis.vertical,
          children: [
            Expanded(
              child: GridView.count(
                shrinkWrap: true,
                crossAxisCount: 2,
                crossAxisSpacing: 15,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                mainAxisSpacing: 10,
                scrollDirection: Axis.vertical,
                children: [
                  CardRun(
                    "Default scenario",
                    () async => navigateTo(context, await init(paramsConnectB())),
                  ),
                  CardRun(
                    "Skip landing page and servicer list",
                    () async => navigateTo(context, await init(paramsSkipBothPages())),
                  ),
                  CardRun(
                    "Skip landing page",
                    () async => navigateTo(context, await init(paramsSkipLandingPage())),
                  ),
                  CardRun(
                    "Servicer ID provided\nwithout skipping landing page",
                    () async => navigateTo(context, await init(paramsServicerIDWithLandingPage())),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
