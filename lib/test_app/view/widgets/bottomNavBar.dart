import 'package:flutter/material.dart';
import 'package:sw_core/tool/inject.dart';

final index = 0.obs;

bottomNavigationBar() => BottomNavigationBar(
      currentIndex: index.value,
      items: _bottomNavigationBarItems(),
      onTap: (int value) => _setIndex(value),
    );

_bottomNavigationBarItems() => [
      const BottomNavigationBarItem(
        icon: Icon(Icons.play_arrow),
        label: "Run",
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.view_module),
        label: "DIM",
      ),
    ];

void _setIndex(int i) => index.value = i;
