import 'package:flutter/material.dart';
import 'package:flutter_sw/test_app/tool/nav.dart';
import 'package:flutter_sw/test_app/view/widgets/markdown_widgets.dart';

tilesDrawer(BuildContext context) => [
      SizedBox(
        height: 80,
      ),
      _tileReleaseNotes(context),
      _tileChangelog(context),
      _tileUsage(context),
    ];

_tileReleaseNotes(BuildContext context) => ListTile(
      title: _newTile(
        Icon(
          Icons.sticky_note_2_sharp,
        ),
        "Release notes",
        Icon(Icons.arrow_forward_ios),
      ),
      onTap: () async => navigateTo(
        context,
        await releaseNotes(),
      ),
    );

_tileChangelog(BuildContext context) => ListTile(
      title: _newTile(
        Icon(
          Icons.change_circle,
        ),
        "Changelog",
        Icon(
          Icons.arrow_forward_ios,
        ),
      ),
      onTap: () async => navigateTo(
        context,
        await changelog(),
      ),
    );

_tileUsage(BuildContext context) => ListTile(
      title: _newTile(
        Icon(
          Icons.mouse,
        ),
        "Usage",
        Icon(
          Icons.arrow_forward_ios,
        ),
      ),
      onTap: () async => navigateTo(
        context,
        await usage(),
      ),
    );

_newTile(Widget icon, String text, Widget leading) => Row(
      children: [
        icon,
        Text(text),
        Expanded(child: Container()),
        leading,
      ],
    );
