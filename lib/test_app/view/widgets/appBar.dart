import 'package:flutter/material.dart';
import 'package:flutter_sw/test_app/tool/storage.dart';
import 'package:flutter_sw/test_app/view/widgets/restart_app_dialog.dart';

appBar(BuildContext context) => AppBar(
      title: const Text("Consumer App"),
      centerTitle: true,
      actions: [
        _switchLog(context),
      ],
      leading: Builder(
        builder: (context) => IconButton(
          icon: const Icon(
            Icons.list,
          ),
          onPressed: () => Scaffold.of(context).openDrawer(),
        ),
      ),
    );

_switchLog(BuildContext context) => Row(
      children: [
        const Text(
          "Log: ",
        ),
        Switch(
          activeColor: Colors.green,
          value: getBoolean(StorageLabel.logEnabled.name) ?? true,
          onChanged: (value) async {
            await saveBoolean(
              StorageLabel.logEnabled.name,
              value,
            );
            await restartAppDialog(
              context,
            );
          },
        ),
      ],
    );
