import 'package:flutter/material.dart';
import 'package:flutter_sw/test_app/init/init_loan_connect.dart';
import 'package:flutter_sw/test_app/tool/nav.dart';
import 'package:flutter_sw/test_app/view/widgets/appBar.dart';
import 'package:flutter_sw/test_app/view/widgets/bottomNavBar.dart';
import 'package:flutter_sw/test_app/view/widgets/cards_run.dart';
import 'package:flutter_sw/test_app/view/widgets/tiles_dim.dart';
import 'package:flutter_sw/test_app/view/widgets/tiles_drawer.dart';
import 'package:sw_component/components/CATE/page_dim.dart';
import 'package:sw_component/components/CATE/page_run.dart';
import 'package:sw_core/entity_theme/enum/transition.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/entity_user/user.dart';
import 'package:sw_core/interface/layout.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_loan_connect/routes/route_servicers.dart';

import '../../init/init_loan_connect.dart';

class TestHome extends StatelessWidget {
  TestHome({Key? key}) : super(key: key);

  final _userData = User().obs;
  final _layoutSelection = Layout.DEFAULT.obs;
  final _partnerNameSelection = "Spinwheel".obs;

  List<Widget> _pages(BuildContext context) => [
        _pageRun(context),
        _pageDim(context),
      ];

  @override
  Widget build(BuildContext context) {
    return Observer(
      () => Scaffold(
        appBar: appBar(context),
        drawer: Drawer(
          child: _drawerBody(
            context,
          ),
        ),
        body: Center(
          child: _pages(context).elementAt(
            index.value,
          ),
        ),
        bottomNavigationBar: bottomNavigationBar(),
      ),
    );
  }

  Widget _pageDim(BuildContext context) => PageDim(tiles(context));

  Widget _pageRun(BuildContext context) => PageRun(
        cardsRun(
          context,
          widgetsProvidedBuilder: (context) => _openPartnerParamsDialog(context),
          onBackLandingPageProvided: () => _resetTestApp(context),
          onBackLayoutB: () => _resetTestApp(context),
          onBackLayoutDefault: () => _resetTestApp(context),
        ),
      );

  //region drawer
  _drawerBody(BuildContext context) => ListView(
        children: _drawerContent(context),
      );

  List<Widget> _drawerContent(BuildContext context) => [
        ...tilesDrawer(context),
        _expansionUser(),
      ];

  _expansionUser() => ExpansionTile(
        backgroundColor: Colors.black,
        leading: Icon(
          Icons.person,
        ),
        collapsedTextColor: Colors.black,
        childrenPadding: EdgeInsets.only(
          left: 17,
          bottom: 17,
          top: 17,
        ),
        expandedAlignment: Alignment.centerLeft,
        expandedCrossAxisAlignment: CrossAxisAlignment.start,
        title: const Text(
          "Logged user data",
          style: TextStyle(fontSize: 20),
          textAlign: TextAlign.left,
        ),
        children: _expansionUserData(),
      );

  List<Widget> _expansionUserData() => [
        _expansionTextData("Champion: ", _userData.value.isChampion?.toString() ?? "Not logged"),
        _expansionTextData("Borrower: ", _userData.value.isBorrower?.toString() ?? "Not logged"),
        _expansionTextData(
            "Registered: ", _userData.value.isRegistered?.toString() ?? "Not logged"),
        _expansionTextData("ID: ", _userData.value.id == "" ? "Not logged" : _userData.value.id),
        _expansionTextData("Partner ID: ", _userData.value.partnerId?.toString() ?? "Not logged"),
        _expansionTextData(
            "External User ID: ", _userData.value.extUserId?.toString() ?? "Not logged"),
      ];

  Widget _expansionTextData(String key, String? value) => Container(
        margin: EdgeInsets.only(left: 4, right: 2),
        child: RichText(
          text: TextSpan(text: key, style: TextStyle(color: Colors.blue, fontSize: 13), children: [
            TextSpan(
                text: value,
                style: TextStyle(
                  color: Colors.white,
                ))
          ]),
        ),
      );
  //endregion drawer

  //region widgets run widgets provided
  _openPartnerParamsDialog(BuildContext context) => Dialog(
        child: Container(
          padding: const EdgeInsets.only(bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.topRight,
                child: IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
                ),
              ),
              _runParamsProvidedLayoutDefault(context),
              _runParamsProvidedLayoutB(context),
            ],
          ),
        ),
      );

  _runParamsProvidedLayoutB(BuildContext context) => ElevatedButton(
        child: Text("Run with layout B"),
        onPressed: () async {
          _layoutSelection.value = Layout.B;
          _partnerNameSelection.value = "Sample Partner";
          navigateAsWidget(
            context,
            await init(
              paramsProvidedCustomLandingPage(
                context,
                partnerName: "Spinwheel",
                layout: Layout.B,
                logo: _logo(),
                iconText0: _iconText0(),
                iconText1: _iconText1(),
                servicerNotFoundIcon: _servicerNotFoundIcon(),
                linkIcon: _linkIcon(),
                title: _title(),
                navButton: _navButton(context),
                disclaimerText: _disclaimerText(),
                transition: SpinTransition.cupertino,
              ),
            ),
            title: "Run partner params provided custom landing page",
            onBackPressed: () {
              _registerController(context);
              _deleteParams(context);
            },
          );
        },
      );

  _runParamsProvidedLayoutDefault(BuildContext context) => ElevatedButton(
        child: Text("Run with layout default"),
        onPressed: () async {
          _layoutSelection.value = Layout.DEFAULT;
          _partnerNameSelection.value = "Spinwheel";
          navigateAsWidget(
            context,
            await init(
              paramsProvidedCustomLandingPage(
                context,
                partnerName: "Spinwheel",
                layout: Layout.DEFAULT,
                logo: _logo(),
                iconText0: _iconText0(),
                iconText1: _iconText1(),
                servicerNotFoundIcon: _servicerNotFoundIcon(),
                linkIcon: _linkIcon(),
                title: _title(),
                navButton: _navButton(context),
                disclaimerText: _disclaimerText(),
                transition: SpinTransition.size,
              ),
            ),
            title: "Run partner params provided custom landing page",
            onBackPressed: () {
              _registerController(context);
              _deleteParams(context);
            },
          );
        },
      );

  _logo() => Image.asset(
        'assets/images/43w36h.png',
      );

  _iconText0() => const Text("Partner Icon Text 0");

  _iconText1() => const Text("Partner Icon Text 1");

  _servicerNotFoundIcon() => Container(
        width: 60,
        height: 60,
        color: Colors.red,
      );

  _linkIcon() => Container(
        width: 60,
        height: 60,
        color: Colors.orange,
      );

  _title() => const Text("Partner title");

  _navButton(BuildContext context) => Observer(
        () => Container(
          width: double.infinity,
          height: _layoutSelection.value == Layout.B ? 48 : kBottomNavigationBarHeight,
          margin: _layoutSelection.value == Layout.B
              ? const EdgeInsets.symmetric(horizontal: 16)
              : null,
          child: ElevatedButton(
            style: _layoutSelection.value == Layout.B
                ? null
                : ElevatedButton.styleFrom(
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.zero,
                      ),
                    ),
                  ),
            child: const Text("Continue"),
            onPressed: () => routeServicers(context),
          ),
        ),
      );

  _disclaimerText() => const Text("Partner disclaimer text");
  //endregion widgets run widgets provided

  _resetTestApp(BuildContext context) {
    _registerController(context);
    _deleteParams(context);
  }

  _registerController(BuildContext context) {
    if (get<IUseCaseAuth>().user.value.extUserId != null) {
      _userData.value = get<IUseCaseAuth>().user.value;
    } else {
      _userData.value = User();
    }
  }

  _deleteParams(BuildContext context) {
    delete<ParamsLoanConnect>();
    Navigator.of(context).pop();
  }
}
