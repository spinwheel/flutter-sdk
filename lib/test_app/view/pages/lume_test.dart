import 'package:flutter/material.dart';
import 'package:flutter_sw/assets/json_themes.dart';
import 'package:flutter_sw/test_app/init/init_loan_connect.dart';
import 'package:flutter_sw/test_app/sample_setup.dart';
import 'package:flutter_sw/test_app/tool/nav.dart';
import 'package:flutter_sw/test_app/tool/test_data.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/request/request_auth_token.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_loan_connect/init_loan_connect.dart';
import 'package:sw_loan_connect/input/event_handler_loan_connect.dart';
import 'package:sw_loan_connect/loan_connect_app.dart';

class LumeTestApp extends MaterialApp {
  @override
  Widget? get home => LumeTest();
}

class LumeTest extends StatefulWidget {
  LumeTest({Key? key}) : super(key: key);

  @override
  _LumeTestState createState() => _LumeTestState();
}

class _LumeTestState extends State<LumeTest> {
  Future triggerConnectApp() async {
    await initLoanConnect(
      theme: loadThemeJson(light: lightTheme, dark: darkTheme),
      params: paramsConnectDefault(),
      auth: RequestAuthToken.pos(
        get<SWEnvironment>() == SWEnvironment.DEV ? remoteHostSandBox : remoteHost,
        extUserID,
        userID,
      ),
      events: handleEvents,
    );
  }

  void handleEvents(EventHandlerLoanConnect handler) {
    handler.onContinue = _onContinue;
  }

  _onContinue() {
    setState(() {});
    print(mounted);
    print(Navigator.of(context).widget.initialRoute);
    Navigator.of(context, rootNavigator: true).popUntil((route) {
      print(route.settings.name);
      return route.isFirst;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: Text("To LoanConnectApp"),
          onPressed: () async {
            await triggerConnectApp().whenComplete(() => navigateTo(context, LoanConnectApp()));
          },
        ),
      ),
    );
  }
}
