import 'package:flutter/material.dart';
import 'package:flutter_sw/assets/json_themes.dart';
import 'package:sw_core/entity_theme/enum/transition.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/entity_theme/theme_pair.dart';
import 'package:sw_core/interface/layout.dart';
import 'package:sw_core/interface/usecase/i_usecase_auth.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/log.dart';
import 'package:sw_loan_connect/input/event_handler_loan_connect.dart';
import 'package:sw_loan_connect/loan_connect_app.dart';

void runConnectApp(ParamsLoanConnect params) {
  put<ParamsLoanConnect>(() => params);
  runApp(
    MaterialApp(
      home: LoanConnectApp(),
      debugShowCheckedModeBanner: false,
    ),
  );
}

ThemePair loadThemeJson({
  Map<String, dynamic>? light = lightTheme,
  Map<String, dynamic>? dark = darkTheme,
}) =>
    ThemePair.fromJson(light ?? {}, dark ?? {});

ParamsLoanConnect params(
  String partnerName,
  Layout layout, {
  Widget logo = const Icon(
    Icons.android_outlined,
    color: Colors.green,
  ),
  SpinTransition transition = SpinTransition.native,
}) =>
    put<ParamsLoanConnect>(
      () => ParamsLoanConnect(
        isNested: false,
        layout: layout,
        partnerName: partnerName,
        logo: logo,
        pageTransition: transition,
      ),
    );

void handleEvents(EventHandlerLoanConnect handler) {
  handler.authStep.listen(_onAuthenticationChanged);
  handler.error.listen(_onError);
  handler.onPolicy = () => wtf("Privacy Policy Clicked");
}

void _onAuthenticationChanged(AuthStep authStep) {
  switch (authStep) {
    case AuthStep.STEP_UNAUTHORIZED:
    case AuthStep.STEP_SECURITY:
    case AuthStep.STEP_AUTHORIZED:
    case AuthStep.AUTH_ERROR:
      wtf(authStep);
  }
}

void _onError(Object e) {
  wtf("ERROR EVENT CAPTURED:\n\n $e");
}
