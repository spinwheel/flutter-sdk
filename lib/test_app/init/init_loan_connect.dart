import 'package:flutter/material.dart';
import 'package:flutter_sw/assets/json_themes.dart';
import 'package:flutter_sw/test_app/sample_setup.dart';
import 'package:flutter_sw/test_app/tool/test_data.dart';
import 'package:sw_core/entity_servicer/spin_servicer.dart';
import 'package:sw_core/entity_theme/enum/transition.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/environment/sw_env.dart';
import 'package:sw_core/interface/layout.dart';
import 'package:sw_core/interface/service/i_service_token.dart';
import 'package:sw_core/request/request_auth_token.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_loan_connect/init_loan_connect.dart';
import 'package:sw_loan_connect/loan_connect_app.dart';
import 'package:sw_loan_connect/routes/route_servicers.dart';
import 'package:sw_core/tool/observer.dart';

import '../tool/storage.dart';
import 'init_custom_layout.dart';

SWEnvironment get _env => SWEnvironment.DEV;

// region builder

Future<Widget> init(
  ParamsLoanConnect params, [
  Map<String, dynamic>? lTheme = lightTheme,
  Map<String, dynamic>? dTheme = darkTheme,
]) async =>
    FutureBuilder(
      future: initLoanConnect(
        params: params,
        auth: RequestAuthToken.pos(
          _env == SWEnvironment.DEV ? remoteHostSandBox : remoteHost,
          extUserID,
          userID,
        ),
        theme: loadThemeJson(light: lTheme, dark: dTheme),
        events: handleEvents,
        enableLog: getBoolean(StorageLabel.logEnabled.name) ?? true,
      ),
      builder: (context, snapshot) {
        resetServiceTheme(lTheme, dTheme);
        return Observer(() => get<IServiceToken>().token.value.isNotEmpty ? LoanConnectApp() : loading(context));
      },
    );

loading(BuildContext context) => Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
          ),
        ),
      ),
    );

// endregion builder

// region Parameters Variants

paramsConnectDefault() => replace<ParamsLoanConnect>(
      () => ParamsLoanConnect(
        partnerName: "Spin",
        layout: Layout.DEFAULT,
        pageTransition: SpinTransition.native,
        logo: Image.asset('assets/images/logo_debug.png'),
        swEnv: _env,
      ),
    );

paramsConnectB() => replace<ParamsLoanConnect>(
      () => ParamsLoanConnect(
        partnerName: "Sample Partner",
        isNested: false,
        hasExitButton: true,
        layout: Layout.B,
        build: null,
        pageTransition: SpinTransition.cupertino,
        title: null,
        logo: Image.asset('assets/images/43w36h.png'),
        iconText0: null,
        iconText1: null,
        linkIcon: null,
        servicerNotFoundIcon: null,
        disclaimerText: null,
        navButton: null,
        swEnv: _env,
      ),
    );

paramsL() => put<ParamsLoanConnect>(
      () => ParamsLoanConnect(
        partnerName: "Purple",
        layout: Layout.DEFAULT,
        logo: Image.asset(
          'assets/images/logo_debug.png',
        ),
        swEnv: _env,
      ),
    );

paramsProvidedLandingPage() => replace<ParamsLoanConnect>(
      () => ParamsLoanConnect(
        partnerName: "Sample Partner",
        layout: Layout.B,
        build: (context) => Scaffold(
          extendBodyBehindAppBar: true,
          extendBody: true,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "This is a screen with navigation.",
                ),
                ElevatedButton(
                  child: Text("Click to navigate"),
                  onPressed: () => routeServicers(context),
                )
              ],
            ),
          ),
        ),
        swEnv: _env,
      ),
    );

paramsProvidedCustomLandingPage(
  BuildContext context, {
  required String partnerName,
  required Layout layout,
  Widget? logo,
  Widget? iconText0,
  Widget? iconText1,
  Widget? servicerNotFoundIcon,
  Widget? linkIcon,
  Widget? title,
  Widget? navButton,
  Widget? disclaimerText,
  SpinTransition? transition,
}) =>
    replace<ParamsLoanConnect>(
      () => ParamsLoanConnect(
        partnerName: partnerName,
        layout: layout,
        logo: logo,
        iconText0: iconText0,
        iconText1: iconText1,
        servicerNotFoundIcon: servicerNotFoundIcon,
        linkIcon: linkIcon,
        title: title,
        navButton: navButton,
        disclaimerText: disclaimerText,
        pageTransition: transition,
      ),
    );

paramsSkipBothPages() => ParamsLoanConnect(
      partnerName: "Sample Partner",
      isNested: false,
      hasExitButton: true,
      layout: Layout.DEFAULT,
      build: null,
      pageTransition: SpinTransition.cupertino,
      title: null,
      logo: Image.asset('assets/images/43w36h.png'),
      iconText0: null,
      iconText1: null,
      linkIcon: null,
      servicerNotFoundIcon: null,
      disclaimerText: null,
      navButton: null,
      skipLandingPage: true,
      skipToServicerID: SpinServicer.STUB_LOAN_SERVICER_SINGLE_STEP,
      swEnv: _env,
    );

paramsSkipLandingPage() => ParamsLoanConnect(
      partnerName: "Sample Partner",
      isNested: false,
      hasExitButton: true,
      layout: Layout.B,
      build: null,
      pageTransition: SpinTransition.cupertino,
      title: null,
      logo: Image.asset('assets/images/43w36h.png'),
      iconText0: null,
      iconText1: null,
      linkIcon: null,
      servicerNotFoundIcon: null,
      disclaimerText: null,
      navButton: null,
      skipLandingPage: true,
      swEnv: _env,
    );

paramsServicerIDWithLandingPage() => ParamsLoanConnect(
      partnerName: "Sample Partner",
      isNested: false,
      hasExitButton: true,
      layout: Layout.B,
      build: null,
      pageTransition: SpinTransition.cupertino,
      title: null,
      logo: Image.asset('assets/images/43w36h.png'),
      iconText0: null,
      iconText1: null,
      linkIcon: null,
      servicerNotFoundIcon: null,
      disclaimerText: null,
      navButton: null,
      skipLandingPage: false,
      skipToServicerID: SpinServicer.STUB_LOAN_SERVICER_SINGLE_STEP,
      swEnv: _env,
    );

//endregion
