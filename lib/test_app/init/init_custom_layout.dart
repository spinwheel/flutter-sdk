import 'package:flutter/material.dart';
import 'package:flutter_sw/assets/json_themes.dart';
import 'package:flutter_sw/test_app/tool/color_picker.dart';
import 'package:flutter_sw/test_app/tool/nav.dart';
import 'package:sw_component/service/service_theme.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/entity_theme/theme_pair.dart';
import 'package:sw_core/interface/service/i_service_theme.dart';
import 'package:sw_core/tool/inject.dart';

import 'init_loan_connect.dart';

final TextEditingController _width = TextEditingController();

//#region init custom layout
_runLoanConnectWidgets(ParamsLoanConnect params, [Map<String, dynamic>? theme]) async =>
    await init(params, theme);

runConfigDialog(ParamsLoanConnect params, BuildContext context) => Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.topRight,
            child: IconButton(
              icon: Icon(Icons.close),
              onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
            ),
          ),
          _widthField(context),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 56,
            width: MediaQuery.of(context).size.width * 0.6,
            child: ColorSelectorDialog(
                color: getSafe<Color>()?.obs ?? put(() => Color(0xFFCCCCCC).obs)),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.6,
            child: ElevatedButton(
              child: Text("Done"),
              onPressed: () async {
                return await _setTheme(params, context);
              },
            ),
          ),
        ],
      ),
    );

_setTheme(ParamsLoanConnect params, BuildContext context) async {
  try {
    double widthParsed = double.parse(_width.text);

    if (widthParsed <= 0) {
      _showScaffoldError(context);
    } else {
      customTheme['inputDecorationTheme']['border']['borderSide']['color'] =
          "#${getSafe<Color>()?.value.toRadixString(16) ?? put(() => Color(0xFFCCCCCC).value.toRadixString(16))}";

      customTheme['inputDecorationTheme']['border']['borderSide']['width'] = widthParsed;
      navigateAsWidget(context, await _runLoanConnectWidgets(params, customTheme),
          title: "Layout widgets", onBackPressed: _resetCustomTheme(context));
    }
  } catch (err, stack) {
    err is FormatException
        ? _showScaffoldError(context)
        : _showScaffoldError(context, message: "An error has ocurred $err, $stack");
  }
}

_showScaffoldError(BuildContext context, {String? message}) =>
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message ?? "Width must be a valid value and cannot be less or equal to zero"),
      behavior: SnackBarBehavior.floating,
    ));

_widthField(BuildContext context) => Container(
      width: MediaQuery.of(context).size.width * 0.6,
      child: TextFormField(
        decoration: InputDecoration(label: Text("Type a width")),
        keyboardType: TextInputType.number,
        controller: _width,
      ),
    );

//#endregion

Map<String, dynamic> customTheme = {
  "colorScheme": {
    "background": "#F5F5F5",
    "brightness": "light",
    "error": "#EF5350",
    "onBackground": "#212121",
    "onError": "#0D47A1",
    "onPrimary": "#FFFFFF",
    "onSecondary": "#212529",
    "onSurface": "#212121",
    "primary": "#1D6AE5",
    "primaryVariant": "#0093c4",
    "secondary": "#03a9f4",
    "secondaryVariant": "#007ac1",
    "surface": "#FFFFFFFF"
  },
  "inputDecorationTheme": {
    "border": {
      "type": "outline",
      "borderRadius": {"type": "circular", "radius": 4.0},
      "borderSide": {"color": "#212529", "style": "solid", "width": 2.0}
    },
    "enabledBorder": {
      "type": "outline",
      "borderRadius": {"type": "circular", "radius": 4.0},
      "borderSide": {"color": "#212529", "style": "solid", "width": 2.0}
    },
  }
};

resetServiceTheme([Map<String, dynamic>? lTheme, Map<String, dynamic>? dTheme]) {
  exists<IServiceTheme>()
      ? replace<IServiceTheme>(
          () => ServiceTheme.fromJson(
            ThemePair(lTheme ?? lightTheme, dTheme ?? darkTheme),
          ),
        )
      : put<IServiceTheme>(
          () => ServiceTheme.fromJson(
            ThemePair(lTheme ?? lightTheme, dTheme ?? darkTheme),
          ),
        );
}

_resetCustomTheme(BuildContext context) {
  delete<IServiceTheme>();
  put<IServiceTheme>(() => ServiceTheme.fromJson(ThemePair(customTheme, darkTheme)));
  Navigator.pop(context);
}
