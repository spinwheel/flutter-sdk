import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_sw/test_app/init/init_loan_connect.dart';
import 'package:flutter_sw/test_app/tool/nav.dart';
import 'package:sw_core/entity_theme/enum/transition.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/interface/layout.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/observer.dart';

_params() => ParamsLoanConnect(
      partnerName: _textPartnerNameValue.text,
      pageTransition: _dropdownTransitionValue.value,
      hasExitButton: _switchHasExitButtonValue.value,
      layout: _dropdownLayoutValue.value,
    );

final Map<String, dynamic> _labels = {
  "Background": Color(0xFF888888).obs,
  "Error": Color(0xFF888888).obs,
  "On background": Color(0xFF888888).obs,
  "On error": Color(0xFF888888).obs,
  "On primary": Color(0xFF888888).obs,
  "On secondary": Color(0xFF888888).obs,
  "On surface": Color(0xFF888888).obs,
  "Primary": Color(0xFF888888).obs,
  "Primary variant": Color(0xFF888888).obs,
  "Secondary": Color(0xFF888888).obs,
  "Secondary variant": Color(0xFF888888).obs,
  "Surface": Color(0xFF888888).obs,
};

final List<DropdownMenuItem<Brightness>> _brightnessValues = [
  DropdownMenuItem(
    child: Text("Light"),
    value: Brightness.light,
  ),
  DropdownMenuItem(
    child: Text("Dark"),
    value: Brightness.dark,
  ),
];

final List<DropdownMenuItem<SpinTransition>> _transitionValues = [
  DropdownMenuItem(
    child: Text("Fade"),
    value: SpinTransition.fade,
  ),
  DropdownMenuItem(
    child: Text("Right to left"),
    value: SpinTransition.rightToLeft,
  ),
  DropdownMenuItem(
    child: Text("Left to right"),
    value: SpinTransition.leftToRight,
  ),
  DropdownMenuItem(
    child: Text("Up to down"),
    value: SpinTransition.topToBottom,
  ),
  DropdownMenuItem(
    child: Text("Down to up"),
    value: SpinTransition.bottomToTop,
  ),
  DropdownMenuItem(
    child: Text("Right to left with fade"),
    value: SpinTransition.rightToLeftWithFade,
  ),
  DropdownMenuItem(
    child: Text("Left to right with fade"),
    value: SpinTransition.leftToRightWithFade,
  ),
  DropdownMenuItem(
    child: Text("Right to left joined"),
    value: SpinTransition.rightToLeftJoined,
  ),
  DropdownMenuItem(
    child: Text("Left to right joined"),
    value: SpinTransition.leftToRightJoined,
  ),
  DropdownMenuItem(
    child: Text("Cupertino"),
    value: SpinTransition.cupertino,
  ),
  DropdownMenuItem(
    child: Text("Fluent"),
    value: SpinTransition.fluent,
  ),
  DropdownMenuItem(
    child: Text("Material"),
    value: SpinTransition.material,
  ),
  DropdownMenuItem(
    child: Text("Rotate"),
    value: SpinTransition.rotate,
  ),
  DropdownMenuItem(
    child: Text("Size"),
    value: SpinTransition.size,
  ),
  DropdownMenuItem(
    child: Text("Scale"),
    value: SpinTransition.scale,
  ),
  DropdownMenuItem(
    child: Text("Native"),
    value: SpinTransition.native,
  ),
];

final List<DropdownMenuItem<Layout>> _layoutValues = [
  DropdownMenuItem(child: Text("Default"), value: Layout.DEFAULT),
  DropdownMenuItem(
    child: Text("B"),
    value: Layout.B,
  ),
];

var _dropdownBrightnessValue = Brightness.light.obs;
var _dropdownTransitionValue = SpinTransition.native.obs;
var _dropdownLayoutValue = Layout.DEFAULT.obs;
final _textPartnerNameValue = TextEditingController();
final _switchHasExitButtonValue = false.obs;

Map<String, dynamic> theme() {
  return {
    "colorScheme": {
      "background": "#${_labels["Background"].value.value.toRadixString(16)}",
      "brightness": _dropdownBrightnessValue == Brightness.light ? "light" : "dark",
      "error": "#${_labels["Error"].value.value.toRadixString(16)}",
      "onBackground": "#${_labels["On background"].value.value.toRadixString(16)}",
      "onError": "#${_labels["On error"].value.value.toRadixString(16)}",
      "onPrimary": "#${_labels["On primary"].value.value.toRadixString(16)}",
      "onSecondary": "#${_labels["On secondary"].value.value.toRadixString(16)}",
      "onSurface": "#${_labels["On surface"].value.value.toRadixString(16)}",
      "primary": "#${_labels["Primary"].value.value.toRadixString(16)}",
      "primaryVariant": "#${_labels["Primary variant"].value.value.toRadixString(16)}",
      "secondary": "#${_labels["Secondary"].value.value.toRadixString(16)}",
      "secondaryVariant": "#${_labels["Secondary variant"].value.value.toRadixString(16)}",
      "surface": "#${_labels["Surface"].value.value.toRadixString(16)}",
    },
    "inputDecorationTheme": {
      "border": {
        "type": "outline",
        "borderRadius": {"type": "circular", "radius": 4.0},
        "borderSide": {"color": "#212529", "style": "solid", "width": 2.0}
      },
      "enabledBorder": {
        "type": "outline",
        "borderRadius": {"type": "circular", "radius": 4.0},
        "borderSide": {"color": "#212529", "style": "solid", "width": 2.0}
      },
    }
  };
}

final _dropdownDecoration = InputDecoration(
  enabledBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      style: BorderStyle.none,
      color: Colors.transparent,
    ),
  ),
);

class CustomLayoutForm extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => _resetValues(context),
          icon: Platform.isIOS ? Icon(Icons.arrow_back_ios_outlined) : Icon(Icons.arrow_back),
        ),
        title: Text("Custom layout"),
        centerTitle: true,
      ),
      body: SafeArea(
        minimum: EdgeInsets.all(15),
        child: Container(
          child: ListView(
            children: [
              Text(
                "ColorScheme configs",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 24),
              ),
              ..._generateColorPickers(context),
              _brightnessTile(context),
              SizedBox(
                height: 20,
              ),
              Text(
                "ParamsLoanConnect configs",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 24),
              ),
              SizedBox(
                height: 12,
              ),
              _pageTransitionTile(context),
              _layoutTile(context),
              _hasExitButtonTile(),
              _textPartnerName(context),
              _continueButton(context),
            ],
          ),
        ),
      ),
    );
  }

  _resetValues(BuildContext context) {
    Navigator.pop(context);
    delete<ParamsLoanConnect>();
  }

  _generateColorPickers(BuildContext context) => List.generate(
        _labels.length,
        (index) => ExpansionTile(
          title: Observer(
            () => Text(
              _labels.keys.elementAt(index),
              style: TextStyle(color: _labels.values.elementAt(index).value),
            ),
          ),
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: Observer(
                () => ColorPicker(
                  hexInputBar: true,
                  pickerColor: _labels.values.elementAt(index).value,
                  onColorChanged: (colorChanged) =>
                      _labels.values.elementAt(index).value = colorChanged,
                ),
              ),
            ),
          ],
        ),
      );

  _brightnessTile(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Brightness"),
            _brightnessDropdown(context),
          ],
        ),
      );

  _brightnessDropdown(BuildContext context) => Container(
        width: 100,
        child: Observer(
          () => DropdownButtonFormField<Brightness>(
            value: _dropdownBrightnessValue.value,
            icon: Icon(Icons.keyboard_arrow_down),
            decoration: _dropdownDecoration,
            alignment: Alignment.centerRight,
            onChanged: (value) =>
                _dropdownBrightnessValue.value = value != null ? value : Brightness.light,
            items: _brightnessValues,
          ),
        ),
      );

  _pageTransitionTile(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Page transition"),
            _pageTransitionDropdown(context),
          ],
        ),
      );

  _pageTransitionDropdown(BuildContext context) => Container(
        width: 190,
        child: Observer(
          () => DropdownButtonFormField<SpinTransition>(
            decoration: _dropdownDecoration,
            value: _dropdownTransitionValue.value,
            icon: Icon(Icons.keyboard_arrow_down),
            alignment: Alignment.centerRight,
            items: _transitionValues,
            onChanged: (value) =>
                _dropdownTransitionValue.value = value != null ? value : SpinTransition.native,
          ),
        ),
      );

  _layoutTile(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Layout"),
            _layoutDropdown(context),
          ],
        ),
      );

  _layoutDropdown(BuildContext context) => Container(
        width: 100,
        child: Observer(
          () => DropdownButtonFormField<Layout>(
            icon: Icon(Icons.keyboard_arrow_down),
            value: _dropdownLayoutValue.value,
            alignment: Alignment.centerRight,
            decoration: _dropdownDecoration,
            items: _layoutValues,
            onChanged: (value) =>
                _dropdownLayoutValue.value = value != null ? value : Layout.DEFAULT,
          ),
        ),
      );

  _hasExitButtonTile() => Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Has exit button (valid for Layout B)"),
            _hasExitButtonSwitch(),
          ],
        ),
      );

  _hasExitButtonSwitch() => Observer(
        () => Switch(
          value: _switchHasExitButtonValue.value,
          onChanged: (value) => _switchHasExitButtonValue.value = value,
          activeColor: Colors.green,
        ),
      );

  _textPartnerName(BuildContext context) => Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          decoration: InputDecoration(hintText: "Partner name"),
          controller: _textPartnerNameValue,
        ),
      );

  _continueButton(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton(
          child: Text("Continue"),
          onPressed: () async => navigateTo(
            context,
            await init(
              _params(),
              theme(),
              theme(),
            ),
          ),
        ),
      );
}
