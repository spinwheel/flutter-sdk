const Map<String, dynamic> sampleTheme = {
  "colorScheme": {
    "background": "#F5F5F5",
    "brightness": "light",
    "error": "#EF5350",
    "onBackground": "#212121",
    "onError": "#0D47A1",
    "onPrimary": "#FFFFFF",
    "onSecondary": "#212529",
    "onSurface": "#212121",
    "primary": "#1D6AE5",
    "primaryVariant": "#0093c4",
    "secondary": "#03a9f4",
    "secondaryVariant": "#007ac1",
    "surface": "#FFFFFFFF"
  },
  "inputDecorationTheme": {
    "border": {
      "type": "outline",
      "borderRadius": {"type": "circular", "radius": 3.0},
      "borderSide": {"color": "#363636", "style": "solid", "width": 1.0}
    },
    "enabledBorder": {
      "type": "outline",
      "borderRadius": {"type": "circular", "radius": 3.0},
      "borderSide": {"color": "#363636", "style": "solid", "width": 1.0}
    },
  },
  "fontFamily": "Helvetica",
  "textTheme": {
    "headline4": {
      "color": "#212121",
      "backgroundColor": "#00000000",
      "fontWeight": "w500",
      "fontStyle": "normal",
      "fontSize": 24.0,
      "height": 1.4,
      "textBaseline": "alphabetic",
      "locale": {"countryCode": "US", "languageCode": "en"}
    },
    "bodyText1": {
      "color": "#212121",
      "backgroundColor": "#00000000",
      "fontSize": 16.0,
      "fontWeight": "w400",
      "fontStyle": "normal",
      "textBaseline": "alphabetic",
      "height": 1.6,
      "locale": {"countryCode": "US", "languageCode": "en"}
    },
    "bodyText2": {
      "color": "#6D6D6D",
      "backgroundColor": "#00000000",
      "fontSize": 14.0,
      "fontWeight": "w400",
      "fontStyle": "normal",
      "textBaseline": "alphabetic",
      "height": 1.5,
      "locale": {"countryCode": "US", "languageCode": "en"}
    },
    "button": {
      "color": "#FFFFFF",
      "backgroundColor": "#00000000",
      "fontSize": 16.0,
      "fontWeight": "w500",
      "fontStyle": "normal",
      "textBaseline": "alphabetic",
      "height": 1.0,
      "locale": {"countryCode": "US", "languageCode": "en"}
    },
  }
};

const Map<String, dynamic> darkTheme = {
  "colorScheme": {
    "background": "#37474f",
    "brightness": "dark",
    "error": "#EF5350",
    "onBackground": "#212121",
    "onError": "#0D47A1",
    "onPrimary": "#FFFFFF",
    "onSecondary": "#4f83cc",
    "onSurface": "#212121",
    "primary": "#0288d1",
    "primaryVariant": "#005b9f",
    "secondary": "#01579b",
    "secondaryVariant": "#002f6c",
    "surface": "#FF62727b"
  },
  "inputDecorationTheme": {
    "border": {
      "type": "outline",
      "borderRadius": {"type": "circular", "radius": 4.0},
      "borderSide": {"color": "#FFF", "style": "solid", "width": 2.0}
    },
    "enabledBorder": {
      "type": "outline",
      "borderRadius": {"type": "circular", "radius": 4.0},
      "borderSide": {"color": "#FFF", "style": "solid", "width": 2.0}
    },
  }
};

const Map<String, dynamic> lightTheme = {
  "colorScheme": {
    "background": "#F5F5F5",
    "brightness": "light",
    "error": "#EF5350",
    "onBackground": "#212121",
    "onError": "#0D47A1",
    "onPrimary": "#FFFFFF",
    "onSecondary": "#212529",
    "onSurface": "#212121",
    "primary": "#1D6AE5",
    "primaryVariant": "#0093c4",
    "secondary": "#03a9f4",
    "secondaryVariant": "#007ac1",
    "surface": "#FFFFFFFF"
  },
  "inputDecorationTheme": {
    "border": {
      "type": "outline",
      "borderRadius": {"type": "circular", "radius": 4.0},
      "borderSide": {"color": "#212529", "style": "solid", "width": 2.0}
    },
    "enabledBorder": {
      "type": "outline",
      "borderRadius": {"type": "circular", "radius": 4.0},
      "borderSide": {"color": "#212529", "style": "solid", "width": 2.0}
    },
  }
};
