# About Spinwheel Tutorial Backend

This repository is designed to showcase a minimalistic implementation of Spinwheel's Drop-In Modules. This repository is a lightweight, single-endpoint, Express backend build to handle the need for server-to-server calls to retrieve the DIM token. It is meant to be used in conjunction with with the Spinwheel Tutorial Frontend.
# Getting Started
## Clone

To begin, open a terminal window and clone this repository onto your local machine using the command:

```
git clone git@bitbucket.org:spinwheel/swsl-tutorial-backend.git
```

Once cloned, cd in the directory like so:

```
cd swsl-tutorial-backend
```
## Starting the Backend Server

To install the node modules for the backend run the npm installation command:
```
npm install
```

Next, you'll need to do is swap out the dummy 'SECRET_KEY' in the .env file with the secretKey you got when you created your account.

Finally, start the backend server with the command:
```
npm start
```

If you've done this correctly, you should see a console log in the terminal that reads: 'Server started on port 8081' (or whatever port you chose if you swapped it out in the env file).

# Using the Application

This application is designed to be used with the swsl-tutorial-frontend to give a quick overview of how to implement Spinwheel drop-in modules. The lone endpoint on this API is a POST request to the root '/'. That endpoint receives either an external user id (extUserId) or a Spinwheel UserId (userId) and returns a DIM token that works for partner to whom the secretKey belongs and the user that's been specified.