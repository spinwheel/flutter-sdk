@echo off

echo ________________________________      DELETE PUBSPEC.LOCK !SDK
call del "pubspec.lock"
echo ________________________________      FLUTTER CLEAN !SDK
call flutter clean
echo ________________________________      FLUTTER PUB UPGRADE !SDK
call flutter pub upgrade
echo ________________________________      FLUTTER PUB GET !SDK
call flutter pub get
exit