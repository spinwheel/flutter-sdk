

# Long Term Styling Support

This document explains the styling/customization context for a cross-platform Flutter app, as well as highlights the differences in comparison to how styling works for web.

## TL;DR
Flutter has its own way of styling screens and components. If used as recommended, styling is simple and straightforward.

Stylings in CSS or HTML directly extracted from web **do not** work without parsing or formatting, which is extensive, prone to bugs and conflicts with Flutter's own recommended approach, rendering it inviable.

Flutter's own recommended approach would be a package of screens and components inherit its consumer app's ThemeData.

The second best approach would be to have the json object match ThemeData's expected values, instead of having to interpret web values into usable mobile format that the ThemeData may not even support the same way as would be expected on web.

This way, we retain default expected functionality, which would be to inherit constumer app's ThemeData, whilst adding an option to provide a json object at compile time which then will construct a ThemeData from the provided json object.


## First, some references...
- [Magenest - Mobile vs Web](https://magenest.com/en/mobile-application-vs-web-application/)
- [Career Foundry - Mobile and Web Pros and Cons](https://careerfoundry.com/en/blog/web-development/what-is-the-difference-between-a-mobile-app-and-a-web-app/)
- [freecodecamp.org - Interpreted vs Compiled](https://www.freecodecamp.org/news/compiled-versus-interpreted-languages/)
- [Medium - Designing for Web vs. Apps in the Mobile Era](https://medium.com/@strv/designing-for-web-vs-apps-in-the-mobile-era-a7c2fff654df)
- [Smashing Magazine - Styling In Modern Web Apps](https://www.smashingmagazine.com/2019/06/styling-modern-web-apps/)
- [Flutter Docs - Widget Catalog](https://docs.flutter.dev/development/ui/widgets)
- [Flutter Docs - Themes](https://docs.flutter.dev/cookbook/design/themes)
- [GSkinner - Custom App Themes](https://blog.gskinner.com/archives/2020/04/flutter-create-custom-app-themes.html)
- [Medium - Flutter design: make your theme homogeneous](https://medium.com/flutter-community/flutter-design-make-your-theme-homogeneous-13ddeffb186f)


# Native ≠ Web

## Native Apps

Native apps work in specific mobile operating systems such as Apple iOS or Android OS. If an app made for Android OS then it will not work on Apple iOS or Windows OS. We have to build separate apps for each operating system if we want to work our app across all major operating systems. This means we have to spend more money and more effort (time, resources).

To mitigate this cost and time spent, we use Flutter. A single code base which compiles to native code for web, desktop and mobile. Different from similar hybrid solutions, Flutter is cross-platform, which means there is **no** "hybridism" or web code creating janky experiences on mobile.

No, Flutter directly compiles and generates native apps, combining the practicity of a single code base with the performance of native apps.

![Flutter Cross Platform](https://bitbucket.org/spinwheel/flutter-sdk/raw/fd45a8880bd4283f50a91699e558f9005a23860a/doc/assets/flutter_build.png)

## Advantages of Native Apps

-   Native apps are faster than web apps.
-   Native apps can access system/device resources such as a GPS or camera.
-   These apps can work without an internet connection.
-   These apps have more safety and security than web apps, as native apps must be approved by the App Store.
-   These apps are easier to develop due to the availability of developer tools, interface elements, and SDKs.

## Disadvantages of Native Apps

-   These apps are more expensive to develop as compared to web apps.
-   Designing and building the app for different platforms such as iOS and Android, is costly and time taking.
-   Maintaining and consistently update of native apps cause more cost.
-   It is difficult to have a native mobile app approved by the App Store.

## Web Apps
Web Apps can be accessed through the mobile device’s Web browser, Web Apps are based on internet-enabled applications. In order to access we don’t need to download and install the app onto a mobile device.

The app is developed as web pages in HTML and CSS, interactive parts are developed in JQuery, JavaScript, or similar language. The single web app can be used on most devices capable of browsing the web, it does not depend upon the operating system they use.

## Advantages of Web Apps

-   Web apps function in-browser, so we do not need to be installed or downloaded.
-   Web apps are easy to maintain, as they have a common codebase regardless of the operating system.
-   These apps can be set to update themselves or automatically.
-   Web Apps easier and faster to build than native mobile apps.
-   In Web Apps App store approval is not required, so web apps can be launched easily.

## Disadvantages of Web Apps

-   Web Apps do not work without an internet connection.
-   Web apps work slower than mobile apps.
-   It is difficult to discover web apps since they aren’t hosted in a specific database like an app store.
-   Web apps have higher risks and poor quality, and there is no guarantee of security since web apps don’t need to be approved by the app stores.


## Difference Between Native Apps and Web Apps
Even though the designs are similar and follow the same color scheme and fonts, these are essentially two different products.

<table><thead><tr><th><p style="text-align:center">Native Apps</p></th><th><p style="text-align:center">Web Apps</p></th></tr></thead><tbody><tr><td><p style="text-align:center">Mobile apps are developed for a specific platform, such as iOS for the Apple iPhone or Android or developed using a cross-platform solution like Flutter.</p></td><td><p style="text-align:center">On the other hand, Web Apps are accessed via the internet browser and will function according to the device you’re viewing them on</p></td></tr><tr><td><p style="text-align:center">They are downloaded and installed via an app store such as Google Play Store and Apple Store and have access to system resources, such as GPS and the camera of the device.</p></td><td><p style="text-align:center">Web apps are not native to a particular system and there is no need to be downloaded or installed.</p></td></tr><tr><td><p style="text-align:center">Mobile apps may work offline.</p></td><td><p style="text-align:center">In order to run web apps need an active internet connection.</p></td></tr><tr><td><p style="text-align:center">Native Apps are comparatively faster.&nbsp;</p></td><td><p style="text-align:center">Web Apps are comparatively slower.</p></td></tr><tr><td><p style="text-align:center">It is difficult to have a native mobile app approved by the App Store.</p></td><td><p style="text-align:center">In Web Apps App store approval is not required, so web apps can be launched easily.</p></td></tr><tr><td><p style="text-align:center">Native apps have more safety and security.</p></td><td><p style="text-align:center">Web apps have comparatively low security.</p></td></tr><tr><td><p style="text-align:center">Maintaining and consistently update of native apps cause more cost.</p></td><td><p style="text-align:center">These apps can be set to update themselves or automatically.</p></td></tr></tbody></table>

# Styling Differences

## Flutter Styling

- [Flutter Docs - Widget Catalog](https://docs.flutter.dev/development/ui/widgets)
- [Flutter Docs - Themes](https://docs.flutter.dev/cookbook/design/themes)
- [GSkinner - Custom App Themes](https://blog.gskinner.com/archives/2020/04/flutter-create-custom-app-themes.html)
- [Medium - Flutter design: make your theme homogeneous](https://medium.com/flutter-community/flutter-design-make-your-theme-homogeneous-13ddeffb186f)

As demonstrated on the references above, Flutter's styling differs from that of the web. Instead of using HTML and CSS, we rely on Flutter Framework's own ThemeData.

This ThemeData object provides several customization options, but these are not thought to receive web values as you would apply them on a CSS or HTML.

Since we are talking about a compiled platform, these need to be fully provided at development time.

**Flutter's quickest out-of-the-box way of theming an underlying package of screens and components would be for said package to simply adhere to the consumer app's ThemeData.**

**This is currently supported by our screens and components.**

Suppose the consumer app's team decides not to use their existing ThemeData, but instead provide through a json object the necessary values for these customizations.

These would work but would need to be mapped to the formats expected by Flutter's ThemeData. Such mapping is not always as simple, so a time cost for this is to be taken into consideration.

![JSON convertion to ThemeData](https://bitbucket.org/spinwheel/flutter-sdk/raw/fd45a8880bd4283f50a91699e558f9005a23860a/doc/assets/json_to_theme.png)

## Web Styling

- [freecodecamp.org - Interpreted vs Compiled](https://www.freecodecamp.org/news/compiled-versus-interpreted-languages/)
- [Medium - Designing for Web vs. Apps in the Mobile Era](https://medium.com/@strv/designing-for-web-vs-apps-in-the-mobile-era-a7c2fff654df)
- [Smashing Magazine - Styling In Modern Web Apps](https://www.smashingmagazine.com/2019/06/styling-modern-web-apps/)

Web styling is heavily based on HTML and CSS. These formats are web-specific, save for a few sub-performatic hybrid solutions like React Native.

These stylings are used for **interpreted apps**, so they are not out-of-the-box compatible with Flutter.

# Our Long-term Support

##  More references
- [raywnderlich - Theming a Flutter App: Getting Started](https://www.raywenderlich.com/16628777-theming-a-flutter-app-getting-started)
- [Digital Ocean - How To Use Themes in Flutter](https://www.digitalocean.com/community/tutorials/flutter-themes)
- [Medium - ThemeData in Flutter](https://medium.com/@omlondhe/themedata-in-flutter-f6a67d9c636d)
- [Flutter Docs - Use themes to share colors and font styles](https://docs.flutter.dev/cookbook/design/themes)

##  ≠ Web Dropin
For long-term support, we are diverging from web dropin's format, since it is web-specific.

![JSON convertion to ThemeData](https://bitbucket.org/spinwheel/flutter-sdk/raw/8b26c7969d33096ebc7f4adca2c91590b3d1884b/doc/assets/color_mapping_1.png)
![JSON convertion to ThemeData](https://bitbucket.org/spinwheel/flutter-sdk/raw/8b26c7969d33096ebc7f4adca2c91590b3d1884b/doc/assets/color_mapping_2.png)

For the Flutter Package approach, we expect a json object which we will parse into a Flutter ThemeData object. ThemeData is extensive, so we will add Flutter-like styling support on demand.

The colors above may be found as the **colorScheme** object from ThemeData.

web|Flutter
-|-
primary-color1|**primary**
primary-color2|**primaryVariant**
primary-color3|**onSurface**
secondary-color1|**secondary**
secondary-color2|**secondaryVariant**
secondary-color3|**onBackground**
background-color|**background**

Above is just a reference of which properties to refer on ColorScheme when trying to match them to the web dropin.

We do support the other properties from ColorScheme in a Flutter-default way, so changing **surface** will change **card colors** and changing **onPrimary** will change the color of a text which is over a primary-colored item.

Below we have a sample of this ThemeData json and the above referenced ColorScheme object and its properties:

```json5

"themeData": {
      "colorScheme": {
        "brightness": "light",
        "primary": "#1D6AE5",
        "primaryVariant": "#0093c4",
        "onPrimary": "#FFFFFF",
        "secondary": "#03a9f4",
        "secondaryVariant": "#007ac1",
        "onSecondary": "#212529",
        "background": "#F5F5F5",
        "onBackground": "#212121",
        "surface": "#FFFFFFFF",
        "onSurface": "#212121",
        "error": "#EF5350",
        "onError": "#0D47A1"
      },
      "inputDecorationTheme": {
        "border": {
          "type": "outline",
          "borderRadius": {
            "type": "circular",
            "radius": 4.0
          },
          "borderSide": {
            "color": "#1F000000",
            "style": "solid",
            "width": 2.0
          }
        },
        "enabledBorder": {
          "type": "outline",
          "borderRadius": {
            "type": "circular",
            "radius": 4.0
          },
          "borderSide": {
            "color": "#1F000000",
            "style": "solid",
            "width": 2.0
          }
        }
      },
      "fontFamily": "Helvetica",
      "textTheme": {
        "headlineMedium": {
            "color": "#212121",
            "backgroundColor": "#00000000",
            "fontWeight": "w500",
            "fontStyle": "normal",
            "fontSize": 24.0,
            "height": 1.4,
            "textBaseline": "alphabetic",
            "locale": {"countryCode": "US", "languageCode": "en"}
        },
        "labelMedium": {
            "color": "#FFFFFF",
            "backgroundColor": "#00000000",
            "fontSize": 16.0,
            "fontWeight": "w500",
            "fontStyle": "normal",
            "textBaseline": "alphabetic",
            "height": 1.0,
            "locale": {"countryCode": "US", "languageCode": "en"}
        },
        "bodyMedium": {
            "color": "#6D6D6D",
            "backgroundColor": "#00000000",
            "fontSize": 14.0,
            "fontWeight": "w400",
            "fontStyle": "normal",
            "textBaseline": "alphabetic",
            "height": 1.5,
            "locale": {"countryCode": "US", "languageCode": "en"}
        },
        "bodyLarge": {
            "color": "#212121",
            "backgroundColor": "#00000000",
            "fontSize": 16.0,
            "fontWeight": "w400",
            "fontStyle": "normal",
            "textBaseline": "alphabetic",
            "height": 1.6,
            "locale": {"countryCode": "US", "languageCode": "en"}
        }
      }
}

```

##  Color Scheme

The central object for defining colors is the ColorScheme object.

Reference: https://api.flutter.dev/flutter/material/ThemeData-class.html

### Primary

[brightness](https://api.flutter.dev/flutter/material/ColorScheme/brightness.html)  →  [Brightness](https://api.flutter.dev/flutter/dart-ui/Brightness.html)  →  The overall brightness of this color scheme, if it's either Light or Dark.

[primary](https://api.flutter.dev/flutter/material/ColorScheme/primary.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  The color displayed most frequently across your app’s screens and components.

[primaryVariant](https://api.flutter.dev/flutter/material/ColorScheme/primaryVariant.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  A darker version of the primary color.

[onPrimary](https://api.flutter.dev/flutter/material/ColorScheme/onPrimary.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  A color that's clearly legible when drawn on  [primary](https://api.flutter.dev/flutter/material/ColorScheme/primary.html).  [[...]](https://api.flutter.dev/flutter/material/ColorScheme/onPrimary.html)

### Secondary

[secondary](https://api.flutter.dev/flutter/material/ColorScheme/secondary.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  An accent color that, when used sparingly, calls attention to parts of your app.

[secondaryVariant](https://api.flutter.dev/flutter/material/ColorScheme/secondaryVariant.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  A darker version of the secondary color.

[onSecondary](https://api.flutter.dev/flutter/material/ColorScheme/onSecondary.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  A color that's clearly legible when drawn on  [secondary](https://api.flutter.dev/flutter/material/ColorScheme/secondary.html).  [[...]](https://api.flutter.dev/flutter/material/ColorScheme/onSecondary.html)

### Background

[background](https://api.flutter.dev/flutter/material/ColorScheme/background.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  A color that typically appears behind scrollable content.

[onBackground](https://api.flutter.dev/flutter/material/ColorScheme/onBackground.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  A color that's clearly legible when drawn on  [background](https://api.flutter.dev/flutter/material/ColorScheme/background.html).  [[...]](https://api.flutter.dev/flutter/material/ColorScheme/onBackground.html)

### Cards and Surfaces

[surface](https://api.flutter.dev/flutter/material/ColorScheme/surface.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  The background color for widgets like  [Card](https://api.flutter.dev/flutter/material/Card-class.html).

[onSurface](https://api.flutter.dev/flutter/material/ColorScheme/onSurface.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  A color that's clearly legible when drawn on  [surface](https://api.flutter.dev/flutter/material/ColorScheme/surface.html).  [[...]](https://api.flutter.dev/flutter/material/ColorScheme/onSurface.html)

### Error

[error](https://api.flutter.dev/flutter/material/ColorScheme/error.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  The color to use for input validation errors, e.g. for  [InputDecoration.errorText](https://api.flutter.dev/flutter/material/InputDecoration/errorText.html).

[onError](https://api.flutter.dev/flutter/material/ColorScheme/onError.html)  →  [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)  →  A color that's clearly legible when drawn on  [error](https://api.flutter.dev/flutter/material/ColorScheme/error.html).  [[...]](https://api.flutter.dev/flutter/material/ColorScheme/onError.html)


## FontFamily

Declare your font in your app's pubspec.yaml as usual. In addition to that, declare your font family, with the very same name, under "fontFamily" on your ThemeData json. Samples can be found at [flutter-sdk/src/dev/lib/assets/json_themes.dart](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/lib/assets/json_themes.dart)

```json5

"themeData": {
      "fontFamily": "Helvetica"
}

```

## TextTheme

Definitions for the various typographical styles found in Material Design. This field is needed to enable font styling capability in order to change its properties like font size, line height et cetera.

```json5
"headlineMedium": {
       "color": "#212121",
       "backgroundColor": "#00000000",
       "fontWeight": "w500",
       "fontStyle": "normal",
       "fontSize": 24.0,
       "height": 1.4,
       "textBaseline": "alphabetic",
       "locale": {"countryCode": "US", "languageCode": "en"}
}

```

### TextTheme properties

[color](https://api.flutter.dev/flutter/painting/TextStyle/color.html) → The color to use when painting the text. → [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)

[backgroundColor](https://api.flutter.dev/flutter/painting/TextStyle/backgroundColor.html) → The color to use as the background for the text. → [Color](https://api.flutter.dev/flutter/dart-ui/Color-class.html)

[fontWeight](https://api.flutter.dev/flutter/painting/TextStyle/fontWeight.html) → The typeface thickness to use when painting the text (e.g., bold). → [FontWeight](https://api.flutter.dev/flutter/dart-ui/FontWeight-class.html)

[fontStyle](https://api.flutter.dev/flutter/painting/TextStyle/fontStyle.html) → The typeface variant to use when drawing the letters (e.g., italics). → [FontStyle](https://api.flutter.dev/flutter/dart-ui/FontStyle.html)

[fontSize](https://api.flutter.dev/flutter/painting/TextStyle/fontSize.html) → The size of glyphs (in logical pixels) to use when painting the text. → [double](https://api.flutter.dev/flutter/dart-core/double-class.html)

[height](https://api.flutter.dev/flutter/painting/TextStyle/height.html) → The height of this text span, as a multiple of the font size. → [double](https://api.flutter.dev/flutter/dart-core/double-class.html)

[textBaseline](https://api.flutter.dev/flutter/painting/TextStyle/textBaseline.html) → The common baseline that should be aligned between this text span and its parent text span, or, for the root text spans, with the line box. → [TextBaseline](https://api.flutter.dev/flutter/dart-ui/TextBaseline.html)

[locale](https://api.flutter.dev/flutter/painting/TextStyle/locale.html) → The locale used to select region-specific glyphs. → [Locale](https://api.flutter.dev/flutter/dart-ui/Locale-class.html)

### TextTheme Sizes

Each of these text styles below is mapped to a Figma specified type of text field. From Figma to Flutter:

- Body 1, 2 and 3 -> bodyLarge, Medium and Small
- Title 1, 2 and 3 -> headlineLarge, Medium and Small
- Button -> labelLarge

#### Displays

 As the largest text on the screen, display styles are reserved for short, important text or numerals. They work best on large screens.

[displayLarge](https://api.flutter.dev/flutter/material/TextTheme/displayLarge.html) → Largest of the display styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[displayMedium](https://api.flutter.dev/flutter/material/TextTheme/displayMedium.html) → Middle size of the display styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[displaySize](https://api.flutter.dev/flutter/material/TextTheme/displaySmall.html) → Smallest of the display styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

#### Headlines

Headline styles are smaller than display styles. They're best-suited for short, high-emphasis text on smaller screens.

[headlineLarge](https://api.flutter.dev/flutter/material/TextTheme/headlineLarge.html) → Largest of the headline styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[headlineMedium](https://api.flutter.dev/flutter/material/TextTheme/headlineMedium.html) → Middle size of the headline styles. →  [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[headlineSmall](https://api.flutter.dev/flutter/material/TextTheme/headlineSmall.html) → Smallest of the headline styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

#### Title

Titles are smaller than headline styles and should be used for shorter, medium-emphasis text.

[titleLarge](https://api.flutter.dev/flutter/material/TextTheme/titleLarge.html) → Largest of the title styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[titleMedium](https://api.flutter.dev/flutter/material/TextTheme/titleMedium.html) → Middle size of the title styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[titleSmall](https://api.flutter.dev/flutter/material/TextTheme/titleSmall.html) → Smallest of the title styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

#### Body

Body styles are used for longer passages of text.

[bodyLarge](https://api.flutter.dev/flutter/material/TextTheme/bodyLarge.html) → Largest of the body styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[bodyMedium](https://api.flutter.dev/flutter/material/TextTheme/bodyMedium.html) → Middle size of the body styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[bodySmall](https://api.flutter.dev/flutter/material/TextTheme/bodySmall.html) → Smallest of the body styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

#### Labels

Label styles are smaller, utilitarian styles, used for areas of the UI such as text inside of components or very small supporting text in the content body, like captions.

Used for text on  [ElevatedButton](https://api.flutter.dev/flutter/material/ElevatedButton-class.html),  [TextButton](https://api.flutter.dev/flutter/material/TextButton-class.html)  and  [OutlinedButton](https://api.flutter.dev/flutter/material/OutlinedButton-class.html).

[labelLarge](https://api.flutter.dev/flutter/material/TextTheme/labelLarge.html) → Largest of the label styles. [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[labelMedium](https://api.flutter.dev/flutter/material/TextTheme/labelMedium.html) → Middle size of the label styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

[labelSmall](https://api.flutter.dev/flutter/material/TextTheme/labelSmall.html) → Smallest of the label styles. → [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html)

## ServiceTheme

ServiceTheme initializes and persists the given theme to style the module adhering to the Flutter Framework's capabilities.

![service_theme_diagram](assets/service_theme_diagram.png)

**JSON** - It will be the JSON with the partner's styling specs.

**ThemePair** - A complete theme, comprised of two variations: a light and a dark theme. Both are expected as json ThemeData objects.

**ServiceTheme** - Persists in memory a reference to the ThemeData objects for widgets reference and consumption. Such ThemeData objects (light and dark) can be provided directly as dart objects or provided as json which will then be parsed into a ThemeData dart object.

**ExtTheme** - Extends upon the current framework's behaviour of ThemeData, allowing for more advanced features such as the json parsing mentioned above. This allows ThemeData objects to be converted to and from json.

**MaterialApp** - The widget at the top of our package's widget-tree. By providing it with the above ThemeData objects, such theming options get propagated down the widget tree, affecting all subsequent widgets.