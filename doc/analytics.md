


# Analytics

Currently we employ Mixpanel's analytics. These are not partner-configurable and statically reference Spinwheel's dashboard.

The core package will check for SWEnvironment and dispatch events accordingly and separately to either DEV or PROD dashboards.

![mixpanel_events.png](assets/mixpanel_events.png)

## Analytics Structure

We chose to create our analytics structure in our Core package, since that automatically adds the same support to each and all packages which use Core.

### Interfaces

Below interfaces specify exactly what kind of behaviour our Analytics and Logger implementation should, at the bare minimum, have.

```dart

abstract class IAnalytics {
  void track(String eventName, {Map<String, dynamic>? properties});
}

abstract class ILogger {
   /// Log a message at level [Level.verbose].
   void v(dynamic message, [dynamic error, StackTrace? stackTrace]);

   /// Log a message at level [Level.debug].
   void d(dynamic message, [dynamic error, StackTrace? stackTrace]);

   /// Log a message at level [Level.info].
   void i(dynamic message, [dynamic error, StackTrace? stackTrace]);

   /// Log a message at level [Level.warning].
   void w(dynamic message, [dynamic error, StackTrace? stackTrace]);

   /// Log a message at level [Level.error].
   void e(dynamic message, [dynamic error, StackTrace? stackTrace]);

   /// Log a message at level [Level.wtf].
   void wtf(dynamic message, [dynamic error, StackTrace? stackTrace]);
}


```

### Implementation of said interfaces

Below we have our Analytics class, a singleton, which will expose the track function internally to our other packages. This class wraps around Mixpanel's implementation, so we don't directly depend on this specific implementation at higher level packages. Instead, other packages will simply rely on Core's Analytics to know the specifics, allowing for a clean separation of concerns.

```dart

import 'package:mixpanel_flutter/mixpanel_flutter.dart';
import 'package:sw_core/environment/sw_env.dart';

import '../interface/i_analytics.dart';
import '../tool/inject.dart';

const _sand = '0f39e35caa5482a0561a682618fa8a54';
const _prod = '7fe2700b39a268128c42afab4534610c';

String get _token => getSafe<SWEnvironment>()?.isProd == true ? _prod : _sand;

class Analytics implements IAnalytics {
  late Mixpanel _mixpanel;

  Analytics({bool optOutTrackingDefault = false, Map<String, dynamic>? superProperties}) {
    Mixpanel.init(
      _token,
      optOutTrackingDefault: optOutTrackingDefault,
      superProperties: superProperties,
    ).then((value) {
      _mixpanel = value;
      replace<IAnalytics>(() => this);
    });
  }

  @override
  void track(String eventName, {Map<String, dynamic>? properties}) async {
    _mixpanel.track(eventName, properties: properties);
  }
}

import 'package:logger/logger.dart';
import 'package:sw_core/interface/i_analytics.dart';
import '../interface/i_logger.dart';
import 'inject.dart';

class SWLogger extends Logger implements ILogger {
   SWLogger()
           : super(
      printer: PrettyPrinter(
         methodCount: 0,
         errorMethodCount: 12,
         lineLength: 77,
         colors: true,
         printTime: false,
         noBoxingByDefault: false,
      ),
   ) {
      replace<ILogger>(() => this);
   }
}

bool get _logEnabled => exists<ILogger>();

bool get _analyticsEnabled => exists<IAnalytics>();

ILogger? get _logger => getSafe<ILogger>();

verbose(dynamic message, [dynamic error, StackTrace? stackTrace]) {
   if (_logEnabled) {
      _logger?.v(message, error, stackTrace);
      _track(message, error, stackTrace);
   }
}

debug(dynamic message, [dynamic error, StackTrace? stackTrace]) {
   if (_logEnabled) {
      _logger?.d(message, error, stackTrace);
      _track(message, error, stackTrace);
   }
}

info(dynamic message, [dynamic error, StackTrace? stackTrace]) {
   if (_logEnabled) {
      _logger?.i(message, error, stackTrace);
      _track(message, error, stackTrace);
   }
}

warning(dynamic message, [dynamic error, StackTrace? stackTrace]) {
   if (_logEnabled) {
      _logger?.w(message, error, stackTrace);
      _track(message, error, stackTrace);
   }
}

error(dynamic message, [dynamic error, StackTrace? stackTrace]) {
   if (_logEnabled) {
      _logger?.e(message, error, stackTrace);
      _track(message, error, stackTrace);
   }
}

errorWithStack(Object error, StackTrace stackTrace) {
   if (_logEnabled) {
      _logger?.e(error.toString(), error, stackTrace);
      _track(error, error, stackTrace);
   }
}

wtf(dynamic message, [dynamic error, StackTrace? stackTrace]) {
   if (_logEnabled) {
      _logger?.wtf(message, error, stackTrace);
      _track(message, error, stackTrace);
   }
}

_track(dynamic message, [dynamic error, StackTrace? stackTrace]) {
   if (_analyticsEnabled) {
      get<IAnalytics>().track(
         message.toString(),
         properties: (error != null || stackTrace != null)
                 ? {
            'error': error,
            'stackTrace': stackTrace,
         }
                 : {},
      );
   }
}


```

Similarly, our SWLogger wraps around the Logger lib, allowing for plug-and-play-ability for both Analytics and Logging capabilities. Each and every function that we're exposing which employs our logger will also notify our IAnalytics implementation and, so long that one has been already initialized, events will be dispatched to Mixpanel's dashboard. 

Both these functionalities rely on a null-safe approach. Should logger have not been enabled by a partner, no logs will be triggered and thus no log event will be dispatched to the dashboard. Should no IAnalytics implementation have been provided previously at setup, also no log will be dispatched to mixpanel's dashboard.

This is by design so that these work exclusively in case we choose to initialize and do provide both a Logger and Analytics implementation. Logs will work normally without Analytics.