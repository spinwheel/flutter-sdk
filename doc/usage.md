


# Usage

## This package

Your app won't depend on this package directly. This package serves both as reference and documentation on the recommended usage of the underlying Spinwheel packages which are referenced on [pubspec.yaml](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/pubspec.yaml).

## Requirements

This Usage Guide uses Android Studio, JDK 8, Flutter SDK and Git.

- [Flutter - getting started](https://docs.flutter.dev/get-started/install).
- [Git - getting started](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Java - downloads](https://www.oracle.com/java/technologies/downloads/)

(Go in the section **Java SE Development Kit 8u311**)


## Cloning the repository

1. Go to [flutter-sdk](https://bitbucket.org/spinwheel/flutter-sdk) repository
2. **Clone** using **HTTPS**
3. Checkout **release** branch (**git checkout release**)

## Running the project

1. Once you open the project in Android Studio, open Run/Debug Configurations panel.
2. In the top-side in left corner, click under the "+" button and select "Flutter"
3. In the "Dart entrypoint" field, select the folder icon
4. In the menu, click in flutter-sdk > **lib > run > run_loan_connect.dart** and hit "OK"
5. In the first field, type a name for your config and hit "Apply" and then "OK"
6. Choose a supported device and run this config. This will execute a sample default way of using the drop-in-module.


## Running it your way

1. On **json_themes.dart** we demonstrate the usage of **resolved json objects**, which is the theme format expected by our packages. You may fetch your theme from an endpoint or load it up from a local asset in your own app's asset folder. Alternatively, you may hardcode it as sampled.
2. In similar manner, on **json_configs.dart** we demonstrate the expected format for non-theming configurables, like layout selection or a custom button you may want to toggle on/off.
3. On **run.dart** you will find the code snippet below which exemplifies how to setup and run the dropin.  
   <br><br>

```dart  

 _init() async => initLoanConnect( 
	 auth: RequestAuthToken.pos(remoteHost, extUserID),
	 theme: loadThemeJson(light: yourTheme, dark: yourDarkTheme),
	 events: handleEvents,
  ); 
            
runLoanConnectDefault() => _init().then(  
	 (widget) => LoanConnectApp(  
		  _params(  
			  ConfigLoanConnect.fromJson(defaultConfig),  
			  logo: Image.asset('assets/images/logo_debug.png'),  
		  ),  
	  ),  
  );
```  

4. **initLoanConnect** function is required before the connect module can run properly.
5. **RequestAuthToken** object requires the express server's address  (the server that handles your Secret Key) and an external user ID.
6. **loadThemeJson** function loads json objects into a ThemePair: a light and a matching equivalent dark theme. These will be internally injected and internally referenced.
7. **handleEvents** is a sample function exemplifying what is the expected syntax for observing and reacting to in-module events.
8. **whenComplete**, runConnectApp will be called. This is another sample function that demonstrates LoanConnectApp running inside a MaterialApp.
9. **params**, again a sample function to demonstrate the creation of a Parameters object.
10. **ParamsLoanConnect** is a required Parameters object and expects a json config object in the format exemplified at json_configs.dart and a logo.
11. **logo** property is where the partner can provide a local asset for its logo as an Image widget. Local or remote, svg or raster, it will work so long it conforms to a default Image widget from the platform. This is an alternative to **imageUrl** which is provided as a partner config and is explained in the Partner Config session below and *will only work if imageUrl is assigned as null*.
12. **LoanConnectApp** is required and depends upon the provided ParamsLoanConnect object to know how to present itself.
13. Referencing and customizing the above required and optional configurations, you may initialize the dropin at any point inside your app.


## Upgrade dependencies

1. Check that you are running using latest version of [Flutter SDK stable channel](https://github.com/flutter/flutter/tree/stable).
2. Access Flutter's cache folder and obliterate **git** folder from existence. This will remove all      
   git-obtained dependencies from cache.
3. Run the following commands or **run_rebuild.bat**, once completed your dependencies will be up-to-date.  
   <br><br>

```dart
    del "pubspec.lock" flutter clean flutter pub upgrade flutter pub get
```

## Authentication

1. Once user successfully logs in, AuthStep.STEP_AUTHORIZED will be posted to authStep observable.
2. At this point, on ControllerAuth you will have a valid **user** observable object holding a valid user value to perform operations with.
3. Optionally and/or depending on your method of navigation, you may want to dispose of the login module entirely once a user object is obtained due to a succesfull login. In such cases, persisting this user object trough your own dependency injection approach is recommended.



## Styling

1. Theming options match those of the web experience, so values like primaryColor and backgroundColor used in our json samples will be applied to the same fields as the web dropin solution.
2. Although sampled as hardcoded json objects, the recommended usage is trough fetching such json from either a local asset in your app or from an endpoint.
3. Current styling does support font family but does not support additional font stylings.
4. Below is a snippet from json_themes.dart where we sample the expected object specifying colors and font family.

```dart

const Map<String, dynamic> sampleTheme = {
   "status": {"code": 200, "desc": "success"},
   "data": {
      "name": "Sample Partner",
      "style": {
         "primary-color1": "#1D6AE5",
         "primary-color2": "#0093c4",
         "primary-color3": "#FFFFFF",
         "secondary-color1": "#03a9f4",
         "secondary-color2": "#007ac1",
         "secondary-color3": "#212529",
         "background-color": "#F5F5F5",
         "font-family": "Helvetica",
         "font-size": 16,
         "card": {
            "default": {
               "bgColor": "#FFFFFF",
               "bgColorOpacity": 100,
               "border": {"type": "solid", "color": "secondary-color3", "size": 1, "opacity": 5},
               "borderRadius": 8
            }
         }
      }
   }
};

const Map<String, dynamic> darkTheme = {
   "status": {"code": 200, "desc": "success"},
   "data": {
      "name": "Spinwheel",
      "style": {
         "primary-color1": "#0288d1",
         "primary-color2": "#005b9f",
         "primary-color3": "#FFFFFF",
         "secondary-color1": "#01579b",
         "secondary-color2": "#002f6c",
         "secondary-color3": "#4f83cc",
         "background-color": "#37474f",
         "font-family": "Inter",
         "font-size": 16,
         "card": {
            "default": {
               "bgColor": "#62727b",
               "bgColorOpacity": 100,
               "border": {"type": "solid", "color": "secondary-color3", "size": 1, "opacity": 5},
               "borderRadius": 8
            }
         }
      }
   }
};

const Map<String, dynamic> lightTheme = {
   "status": {"code": 200, "desc": "success"},
   "data": {
      "name": "Spinwheel",
      "style": {
         "primary-color1": "#1D6AE5",
         "primary-color2": "#0093c4",
         "primary-color3": "#FFFFFF",
         "secondary-color1": "#03a9f4",
         "secondary-color2": "#007ac1",
         "secondary-color3": "#212529",
         "background-color": "#F5F5F5",
         "font-family": "Karla",
         "font-size": 16,
         "card": {
            "default": {
               "bgColor": "#FFFFFF",
               "bgColorOpacity": 100,
               "border": {"type": "solid", "color": "secondary-color3", "size": 1, "opacity": 5},
               "borderRadius": 8
            }
         }
      }
   }
};

```  


## Partner Configs

```dart
import 'package:flutter/material.dart';
import 'package:sw_core/entity_theme/enum/transition.dart';
import 'package:sw_core/interface/layout.dart';

class ParamsLoanConnect {
   ParamsLoanConnect({
      required String partnerName,
      bool isNested = false,
      bool hasExitButton = true,
      Layout layout = Layout.DEFAULT,
      Widget Function(BuildContext)? build,
      SpinTransition? pageTransition,
      Widget? logo,
      Widget? servicerNotFoundIcon,
      Widget? title,
      Widget? iconText0,
      Widget? iconText1,
      Widget? linkIcon,
      Widget? disclaimerText,
      Widget? navButton,
      bool skipLandingPage = false,
      String? skipToServicerID,
      SWEnvironment? swEnv,
   })  : this.partnerName = partnerName,
              this.isNested = isNested,
              this.hasExitButton = hasExitButton,
              this.layout = layout,
              this.build = build,
              this.pageTransition = pageTransition ?? SpinTransition.native,
              this.logo = logo ?? _alert,
              this.servicerNotFoundIcon = servicerNotFoundIcon ?? _alert,
              this.title = title,
              this.iconText0 = iconText0,
              this.iconText1 = iconText1,
              this.linkIcon = linkIcon,
              this.disclaimerText = disclaimerText,
              this.navButton = navButton,
              this.skipToServicerID = skipToServicerID,
              this.skipLandingPage = skipLandingPage,
              this.swEnv = put<SWEnvironment>(() => swEnv ?? SWEnvironment.DEV);

   String partnerName;
   bool isNested;
   bool hasExitButton;
   Layout layout;
   Widget Function(BuildContext)? build;
   SpinTransition pageTransition;
   Widget logo;
   Widget servicerNotFoundIcon;
   Widget? title;
   Widget? iconText0;
   Widget? iconText1;
   Widget? linkIcon;
   Widget? disclaimerText;
   Widget? navButton;
   bool skipLandingPage;
   String? skipToServicerID;
   SWEnvironment swEnv;
}

Widget get _alert => const Icon(Icons.report_problem_outlined, color: Colors.purple);

```

## Error Handling

1. During the authentication process, many errors may occur. From a simple connectivity issue, to a user/pass being invalid or a server-side hiccup.
2. Any error that pops during the authentication, **even those we do handle ourselves like a token expired error**, is being exposed trough the error property on the EventHandler.  
   <br><br>
```dart
    Rx<Object> error = Object().obs;  
```  

3. Simply check trough a switch or if statements which is the error type you want to expect and react to, just like we did on our ResponseHandler:  
   <br><br>
```dart
    _onError(Object object) {
    switch (object.runtimeType) {
      case DioError:
        final errorResponse = (object as DioError).response;
        error("ERROR:\n\n ${errorResponse?.statusCode} -> ${errorResponse?.statusMessage}\n\n");
        break;
      default:
    }
  }
```