# Long Term Customization Support

This document explains the configuration/customization long-term approach for the app-dropin packages.

## TL;DR

We originally started with a json object approach which quickly brought up a series of **platform limitations** when it came to asset resolution.

In short, asset resolution is too much prone to bugs and demands way too much configuration if a package A needs to provide a package B's assets to a package C which will then find this asset.

Instead of expecting to be told where to *find* the asset, we now expect that the partner will provide not a path but the widget itself with an **already resolved local or remote asset**.

This both gives the partner more control over which asset from wherever he wants to provide it and simplifies the setup into fewer lines of code, while also being more reliable thus rendering us less corner-case bugs.

In similar fashion, other customizable items which demand lots of customization are being exposed through the **ParamsLoanConnect** object, the idea being that the partner may either:

- provide nothing and use the default behaviour
- provide a Widget, with its own padding, margin, decoration et cetera
- provide a build function which constructs the entire screen

With these varying degrees of control, the partner may have greater control of the screen but with a longer setup **or** a simple content change with little setup.

# Module: Connect

## Parameters

Below we have the current ParamsLoanConnect object

```dart
import 'package:flutter/material.dart';
import 'package:sw_core/entity_theme/enum/transition.dart';
import 'package:sw_core/interface/layout.dart';

class ParamsLoanConnect {
  ParamsLoanConnect({
    required String partnerName,
    bool isNested = false,
    bool hasExitButton = true,
    Layout layout = Layout.DEFAULT,
    Widget Function(BuildContext)? build,
    SpinTransition? pageTransition,
    Widget? logo,
    Widget? servicerNotFoundIcon,
    Widget? title,
    Widget? iconText0,
    Widget? iconText1,
    Widget? linkIcon,
    Widget? disclaimerText,
    Widget? navButton,
    bool skipLandingPage = false,
    String? skipToServicerID,
    SWEnvironment? swEnv,
  })  : this.partnerName = partnerName,
        this.isNested = isNested,
        this.hasExitButton = hasExitButton,
        this.layout = layout,
        this.build = build,
        this.pageTransition = pageTransition ?? SpinTransition.native,
        this.logo = logo ?? _alert,
        this.servicerNotFoundIcon = servicerNotFoundIcon ?? _alert,
        this.title = title,
        this.iconText0 = iconText0,
        this.iconText1 = iconText1,
        this.linkIcon = linkIcon,
        this.disclaimerText = disclaimerText,
        this.navButton = navButton,
        this.skipToServicerID = skipToServicerID,
        this.skipLandingPage = skipLandingPage,
        this.swEnv = put<SWEnvironment>(() => swEnv ?? SWEnvironment.DEV);

  String partnerName;
  bool isNested;
  bool hasExitButton;
  Layout layout;
  Widget Function(BuildContext)? build;
  SpinTransition pageTransition;
  Widget logo;
  Widget servicerNotFoundIcon;
  Widget? title;
  Widget? iconText0;
  Widget? iconText1;
  Widget? linkIcon;
  Widget? disclaimerText;
  Widget? navButton;
  bool skipLandingPage;
  String? skipToServicerID;
  SWEnvironment swEnv;
}

Widget get _alert => const Icon(Icons.report_problem_outlined, color: Colors.purple);


```

![Landing Page Customizable Fields](assets/landing_page_customizable_fields.png)

Above Widget fields are mapped to our layout in a manner that if nothing is provided the default behaviour kicks in.

```dart
	  Widget? title;  
	  Widget logo;  
	  Widget? iconText0;  
	  Widget? iconText1;  
	  Widget? linkIcon;  
	  Widget? servicerNotFoundIcon;  
	  Widget? disclaimerText;  
	  Widget? navButton;
```

If any of the widgets above is provided when customizing your Parameters object below, that widget will replace the referenced box. Doing this, you can override sections of the screen with whatever widget you see fit, having full control over it.

![Params Skip Both Pages](assets/params.png)

**partnerName** - How the partner's name will be displayed across the module.

**isNested** - Default false. Toggles the screen behaviours between a stand-alone screen *or* a nested widget.

**hasExitButton** - Default true. Toggles a header with an *X* exit/close button.

**layout** - Allows choosing among one of the pre-defined layouts ([core/interface/layout.dart](https://bitbucket.org/spinwheel/flutter-core/src/dev/lib/interface/layout.dart)).

**build** - A callback with BuildContext as an argument that is passed so the partner can provide its own screen. Works in the same way as any other widget build function and overrides all other custom widget fields below.

**pageTransition** - Default *native*. Allows choosing a page transition effect based on common pre-defined ones ([core/entity_theme/enum/transition](https://bitbucket.org/spinwheel/flutter-core/src/dev/lib/entity_theme/enum/transition.dart)).

**title** - Default null. The title's area box. Can be left null for no customization or can be replaced with a widget completely built by the partner.

**logo** - Default ⚠️. Needs to be replaced with an image widget provided by the partner.

**iconText0** - Upper icon+text area box. Can be left null for no customization or can be replaced with a widget completely built by the partner.

**iconText1** - Lower icon+text area box. Can be left null for no customization or can be replaced with a widget completely built by the partner.

**linkIcon** - Widget between the Spinwheel and partner's logo. (Layout.B only)

**servicerNotFoundIcon** - Servicer not found icon after filling the Un-integrated Login form.(Layout.B only)

**disclaimerText** - The bottom disclaimer area box. Can be left null for no customization or can be replaced with a widget completely built by the partner.

**navButton** - Navigation button area box. Can be left null for no customization or can be replaced with a widget completely built by the partner.

**skipLandingPage** - Default false

**skipToServicerID** - Default null (expects a text ID)

**swEnv** - Default SWEnvironment.DEV. Changes between development and production environment.