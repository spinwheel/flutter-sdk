# Release Notes

For full in-detail view of the changes, please consult commits page:      
https://bitbucket.org/spinwheel/flutter-sdk/commits/branch/release

# 2022

## 0.4.3 - February 23

This release addresses several navigation improvements as well as allow for partners to skip pages as they feel the need to do so. Landing Page and Servicers' list may be skipped independently.

Additionally, several layout adjustments, asset removals and bugfixes were resolved.


## 0.4.2 - February 16

Main purpose of this release was to add retro-compatible TestTheme support which can support both 2.10 down to 2.8 Flutter Framework versions. This came as a partner request in order to conform to their version.


## 0.4.1 - February 10

This release was focused on styling and customization support:

We added proper text theming support, which allows mapping of certain field-types globally (heading, bodyText, label et cetera).

Additionally, several improvements to customization options were implemented, allowing far more control over customizable elements whilst reducing the amount of potential bugs, since we are trying to adhere to a more Flutter-like style of code, so our code looks more like you would expect from the Framework itself, thus having a better self-doc presentation.

Due to the several changes and the ongoing incremental nature of the theming options added, documents under https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/ will continue to be updated progressively, so may be considered *live* documents which will always reflect the current state of that said commit/version of the package.


## 0.4.0 - February 2

Mainly we had changes to how we handle styling and partner configurations, after partner feedback. There were some layout adjustments in order for a more standardized experience.

### Partner Configurations:

- Added setup option to toggle on/off the logging capability. This way, at setup, partner may choose to have a logger running or not, so that a release version doesn't "overshare" to a user what is going on.
- As we simplified certain configurables, like a partner logo, and applied a Flutter-friendlier approach we asserted that json config files or hardcoded properties became way shorter, nearing deprecation. These may be discontinued in favor of a similar approach to how we solved asset handling by giving the partner more options and control, **albeit with a little more setup before running the module**
- Docs: added [lts_config.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/lts_configs.md) regarding long-term support for partner configurations. Currently in-progress as these changes are quite recent.

### Styling:

- Styling change for better support from Flutter Framework . As mentioned above, we are diverging from expecting a web-like json object to, instead, expecting a Flutter-oriented json object already referencing how Flutter handles themes through a ThemeData object.
- Added sample_style.json in assets to serve as a theming approach sample
- More details can be checked at [lts_styling.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/lts_styling.md)

### Layouts:

- We elaborate on how Flutter handles polimorphic layouts compared to how web handles the same at [lts_layout.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/lts_layout.md)

### Publishing

- There are pros an cons when working with either pub.dev or a self hosted pub server and we compared them on [publishing.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/publishing.md)

## 0.3.0 - January 26

The main aspect of this release is the **C**onsumer **A**pp **T**esting **E**nvironment, fondly called **CATE**.

CATE helps us exercise whatever test scenarios we need with some freedom, since this app **consumes and showcases** the modules usage in an approximation to what would be the partner's experience when doing the same.

The usage doc documents whatever is not already self-documented in CATE and a **Doc** tab may be found in-app and references all current up-to-date documentation, for a better integration and ease-of-use.

As part of our implementations, we need to add corresponding toggles or tooling to CATE, either just a simple button to some specific screen flow or an option screen for style changes, to validate customizability.

**Other relevant items are**:

- Simplification of our dependency injection approach, aiming at a broader long-term support
- After partner feedback, changes to how partner may provide assets like images for customizable fields
- [lts_styling.md](https://bitbucket.org/spinwheel/flutter-sdk/src/dev/doc/lts_styling.md) regarding long-term support for theming
- We implemented the unintegrated login flow, which provides the expected feedback to the user
- Proper date-pickers for date of birth fields
- Better error feedback and error event exposition so that the partner may react accordingly

# 2021

## 0.2.0 - December 27

0.2.0 aims at making flutter-sdk package customer agnostic. That means this package contains no partner-specific data and can be used to showcase default expected behaviour of these packages in a Spinwheel mock environment.

In addition, provides broader customization and event handling.

## 0.1.0 - December 1

Hello world.

This initial release provides fully functional loan_connect using underlying core, component, data and auth packages.