

# Publishing

Pros and cons of publishing to pub.dev in comparison to a self-hosted private Dart Pub Server.

## TL;DR

There is no loss in using a self-hosted private **D**art **P**ub **S**erver *before* going irreversably public on pub.dev.

Should it still be necessary to publicly publish some or all packages on pub.dev, it can be done later without restrictions.

**Recommendation: anything that is still under development should be published to a self-hosted private DPS.**

**Once UAT indicates the package is stable with enough features, it *can* be published to pub.dev but our recommendation is to keep on using DPS.**


## First, some references...
- [Dart Archive - pub.dev server github repo](https://github.com/dart-archive/pub-dartlang)
- [Dart Dev - Publishing](https://dart.dev/tools/pub/publishing)
- [Dart Dev - Dependencies](https://dart.dev/tools/pub/dependencies)
- [cloudsmith - Dart Repository Hosting](https://cloudsmith.com/dart-repository/)

# Self-Hosted Dart Pub Server

A DPS would have about the same usage and technical criteria as publishing to pub.dev, but without the un-publishing restrictions and without the need to retract or mark something as discontinued, since we can simply remove said package at will.

There are no drawbacks to using a DPS, except that it would need to be setup and maintained, much like any repository in GitHub or Bitbucket.

# pub.dev - Publishing is forever

<p>Keep in mind that publishing is forever. As soon as you publish your package,
users can depend on it. Once they start doing that, removing
the package would break theirs. To avoid that, the <a href="https://pub.dev/policy" class="external">pub.dev policy</a>
disallows unpublishing packages except for very few cases.</p>

<p>You can always upload new versions of your package, but
old ones will continue to be available for users that aren’t ready to
upgrade yet.</p>

<p>For already published packages that are no longer relevant or being maintained,
you can <a href="#discontinue">mark them as discontinued</a>.</p>

<h2 id="preparing-to-publish">
<a class="anchor" href="#preparing-to-publish" aria-hidden="true"><span class="octicon octicon-link"></span></a>Preparing to publish</h2>

<p>When publishing a package, it’s important to follow the <a href="/tools/pub/pubspec">pubspec
format</a> and
<a href="/tools/pub/package-layout">package layout conventions</a>.
Some of these are required in order for others to be able to use your package.
Others are suggestions to help make it easier for users to understand and work
with your package. In both cases, pub tries to help you by pointing out what
changes will help make your package play nicer with the Dart ecosystem. There
are a few additional requirements for uploading a package:</p>

<ul>
  <li>
    <p>You must include a <code class="language-plaintext highlighter-rouge">LICENSE</code> file.
We recommend the <a href="https://opensource.org/licenses/BSD-3-Clause" class="external">BSD 3-clause license</a>,
which the Dart and Flutter teams typically use.
However, you can use any license that’s appropriate for your package.
You must also have the legal right to
redistribute anything that you upload as part of your package.</p>
  </li>
  <li>
    <p>Your package must be smaller than 100 MB after gzip compression. If
it’s too large, consider splitting it into multiple packages, using a
<code class="language-plaintext highlighter-rouge">.pubignore</code> file to remove unnecessary content, or cutting down
on the number of included resources or examples.</p>
  </li>
  <li>
    <p>Your package should depend only on hosted dependencies (from the default pub
package server) and SDK dependencies (<code class="language-plaintext highlighter-rouge">sdk: flutter</code>). These restrictions
ensure that dependencies of your packages cannot become unavailable in the
future.</p>
  </li>
  <li>
    <p>You must have a <a href="https://support.google.com/accounts/answer/27441" class="external">Google Account</a>,
which pub uses to manage package upload permissions.
Your Google Account can be associated with a Gmail address or
with any other email address.</p>
  </li>
</ul>

<h3 id="important-files">
<a class="anchor" href="#important-files" aria-hidden="true"><span class="octicon octicon-link"></span></a>Important files</h3>

<p>Pub uses the contents of a few files to create a page for your
package at <code class="language-plaintext highlighter-rouge">pub.dev/packages/&lt;your_package&gt;</code>. Here are the files that
affect how your package’s page looks:</p>

<ul>
  <li>
<strong>README.md:</strong> The <code class="language-plaintext highlighter-rouge">README.md</code> file
is the main content featured in your package’s page.
The file’s contents are rendered as <a href="https://pub.dev/packages/markdown" class="external">Markdown.</a>
For guidance on how to write a great README, see
<a href="/guides/libraries/writing-package-pages">Writing package pages</a>.</li>
  <li>
<strong>CHANGELOG.md:</strong> Your package’s <code class="language-plaintext highlighter-rouge">CHANGELOG.md</code> file, if found,
is also featured in a tab on your package’s page,
so that developers can read it right from pub.dev.
The file’s contents are rendered as <a href="https://pub.dev/packages/markdown" class="external">Markdown.</a>
</li>
  <li>
<strong>The pubspec:</strong> Your package’s <code class="language-plaintext highlighter-rouge">pubspec.yaml</code> file is used to fill out
details about your package on the right side of your package’s page, like its
description, homepage, etc.</li>
</ul>

<h3 id="verified-publisher">
<a class="anchor" href="#verified-publisher" aria-hidden="true"><span class="octicon octicon-link"></span></a>Advantages of using a verified publisher</h3>

<p>You can publish packages using either a verified publisher (recommended)
or an independent Google Account.
Using a verified publisher has the following advantages:</p>

<ul>
  <li>The consumers of your package know that the publisher domain has been verified.</li>
  <li>You can avoid having pub.dev display your personal email address.
Instead, pub.dev displays the publisher domain and contact address.</li>
  <li>A verified publisher badge <img src="https://dart.dev/assets/img/verified-publisher.svg" alt="pub.dev verified publisher logo"> is displayed next to your package name on both search pages and individual package pages.</li>
</ul>

<h3 id="create-verified-publisher">
<a class="anchor" href="#create-verified-publisher" aria-hidden="true"><span class="octicon octicon-link"></span></a>Creating a verified publisher</h3>

<p>To create a verified publisher, follow these steps:</p>

<ol>
  <li>
    <p>Go to <a href="https://pub.dev" class="external">pub.dev.</a></p>
  </li>
  <li>
    <p>Log in to pub.dev using a Google Account.</p>
  </li>
  <li>
    <p>In the user menu in the top-right corner, select <strong>Create Publisher</strong>.</p>
  </li>
  <li>
    <p>Enter the domain name that you want to associate with your publisher (for example,
<code class="language-plaintext highlighter-rouge">dart.dev</code>), and click <strong>Create Publisher</strong>.</p>
  </li>
  <li>
    <p>In the confirmation dialog, select <strong>OK</strong>.</p>
  </li>
  <li>
    <p>If prompted, complete the verification flow, which opens the <a href="https://search.google.com/search-console/about" class="external">Google
Search Console.</a></p>
    <ul>
      <li>When adding DNS records, it may take a few hours before the Search Console
reflects the changes.</li>
      <li>When the verification flow is complete, return to step 4.</li>
    </ul>
  </li>
</ol>

<h2 id="publishing-your-package">
<a class="anchor" href="#publishing-your-package" aria-hidden="true"><span class="octicon octicon-link"></span></a>Publishing your package</h2>

<p>Use the <a href="/tools/pub/cmd/pub-lish"><code class="language-plaintext highlighter-rouge">dart pub publish</code></a> command to publish your package for the first time,
or to update it to a new version.</p>

<h3 id="performing-a-dry-run">
<a class="anchor" href="#performing-a-dry-run" aria-hidden="true"><span class="octicon octicon-link"></span></a>Performing a dry run</h3>

<p>To test how <code class="language-plaintext highlighter-rouge">dart pub publish</code> will work, you can perform a dry run:</p>

<div class="language-terminal highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="gp">$</span><span class="w"> </span>dart pub publish <span class="nt">--dry-run</span>
</code></pre></div></div>

<p>Pub makes sure that your package follows the
<a href="/tools/pub/pubspec">pubspec format</a> and
<a href="/tools/pub/package-layout">package layout conventions</a>,
and then uploads your package to <a href="https://pub.dev" class="external">pub.dev.</a> Pub also shows you all of
the files it intends to publish. Here’s an example of publishing a package
named <code class="language-plaintext highlighter-rouge">transmogrify</code>:</p>

<pre class="console-output"><code class="language-nocode">Publishing transmogrify 1.0.0
    .gitignore
    CHANGELOG.md
    README.md
    lib
        transmogrify.dart
        src
            transmogrifier.dart
            transmogrification.dart
    pubspec.yaml
    test
        transmogrify_test.dart

Package has 0 warnings.
</code></pre>

<h3 id="publishing">
<a class="anchor" href="#publishing" aria-hidden="true"><span class="octicon octicon-link"></span></a>Publishing</h3>

<p>When you’re ready to publish your package, remove the <code class="language-plaintext highlighter-rouge">--dry-run</code> argument:</p>

<div class="language-terminal highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="gp">$</span><span class="w"> </span>dart pub publish
</code></pre></div></div>

<aside class="alert alert-info" role="alert">
  <p><strong>Note:</strong>
  The pub command currently doesn’t support publishing a new package directly to a
  verified publisher. As a temporary workaround, publish new packages to a Google Account,
  and then <a href="#transferring-a-package-to-a-verified-publisher">transfer the package to a publisher</a>.</p>

  <p>Once a package has been transferred to a publisher,
  you can update the package using <code class="language-plaintext highlighter-rouge">dart pub publish</code>.</p>
</aside>

<p>After your package has been successfully uploaded to pub.dev, any pub user can
download it or depend on it in their projects. For example, if you just
published version 1.0.0 of your <code class="language-plaintext highlighter-rouge">transmogrify</code> package, then another Dart
developer can add it as a dependency in their <code class="language-plaintext highlighter-rouge">pubspec.yaml</code>:</p>

<div class="language-yaml highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="na">dependencies</span><span class="pi">:</span>
  <span class="na">transmogrify</span><span class="pi">:</span> <span class="s">^1.0.0</span>
</code></pre></div></div>

<h3 id="transferring-a-package-to-a-verified-publisher">
<a class="anchor" href="#transferring-a-package-to-a-verified-publisher" aria-hidden="true"><span class="octicon octicon-link"></span></a>Transferring a package to a verified publisher</h3>

<p>To transfer a package to a verified publisher,
you must be an <a href="#uploaders">uploader</a> for the package
and an admin for the verified publisher.</p>

<aside class="alert alert-info" role="alert">
  <p> <strong>Note:</strong>
  This process isn’t reversible. Once you transfer a package to a publisher,
  you can’t transfer it back to an individual account.</p>
</aside>

<p>Here’s how to transfer a package to a verified publisher:</p>

<ol>
  <li>Log in to <a href="https://pub.dev" class="external">pub.dev</a> with a Google Account that’s listed as
an uploader of the package.</li>
  <li>Go to the package details page (for example,
<code class="language-plaintext highlighter-rouge">https://pub.dev/packages/http</code>).</li>
  <li>Select the <strong>Admin</strong> tab.</li>
  <li>Enter the name of the publisher, and click <strong>Transfer to Publisher</strong>.</li>
</ol>

<h2 id="what-files-are-published">
<a class="anchor" href="#what-files-are-published" aria-hidden="true"><span class="octicon octicon-link"></span></a>What files are published?</h2>

<p><strong>All files</strong> under the package root directory are
included in the published package,
with the following exceptions:</p>

<ul>
  <li>Any <em>hidden</em> files or directories —
that is, files with names that begin with dot (<code class="language-plaintext highlighter-rouge">.</code>)</li>
  <li>Any directories with the name <code class="language-plaintext highlighter-rouge">packages</code>
</li>
  <li>Files and directories ignored by a <code class="language-plaintext highlighter-rouge">.pubignore</code> or <code class="language-plaintext highlighter-rouge">.gitignore</code> file</li>
</ul>

<aside class="alert alert-info" role="alert">
  <p><strong>Version note:</strong>
  Support for <code class="language-plaintext highlighter-rouge">.pubignore</code> files was added in Dart 2.14.</p>
</aside>

<p>If you want different ignore rules for <code class="language-plaintext highlighter-rouge">git</code> and <code class="language-plaintext highlighter-rouge">dart pub publish</code>,
then overrule the <code class="language-plaintext highlighter-rouge">.gitignore</code> file in a given directory by
creating a <code class="language-plaintext highlighter-rouge">.pubignore</code> file.
(If a directory contains both a <code class="language-plaintext highlighter-rouge">.pubignore</code> file and a <code class="language-plaintext highlighter-rouge">.gitignore</code> file,
then  <code class="language-plaintext highlighter-rouge">dart pub publish</code> doesn’t read that directory’s <code class="language-plaintext highlighter-rouge">.gitignore</code> file.)
The format of <code class="language-plaintext highlighter-rouge">.pubignore</code> files is the same as the
<a href="https://git-scm.com/docs/gitignore#_pattern_format" class="external"><code class="language-plaintext highlighter-rouge">.gitignore</code> file format</a>.</p>

<p>To avoid publishing unwanted files,
follow these practices:</p>

<ul>
  <li>Either delete any files that you don’t want to include,
or add them to a <code class="language-plaintext highlighter-rouge">.pubignore</code>  or <code class="language-plaintext highlighter-rouge">.gitignore</code> file.</li>
  <li>When uploading your package,
carefully examine the list of files that
<code class="language-plaintext highlighter-rouge">dart pub publish</code> says it’s going to publish.
Cancel the upload if any undesired files appear in that list.</li>
</ul>

<h2 id="uploaders">
<a class="anchor" href="#uploaders" aria-hidden="true"><span class="octicon octicon-link"></span></a>Uploaders</h2>

<p>Whoever publishes the first version of a package automatically becomes
the first and only person authorized to upload additional versions of that package.
To allow or disallow other people to upload versions,
use the <a href="/tools/pub/cmd/pub-uploader"><code class="language-plaintext highlighter-rouge">dart pub uploader</code></a> command
or transfer the package to a <a href="/tools/pub/verified-publishers">verified publisher</a>.</p>

<p>If a package has a verified publisher,
then the pub.dev page for that package displays the publisher domain.
Otherwise, the page displays the email addresses of
the authorized uploaders for the package.</p>

<h2 id="publishing-prereleases">
<a class="anchor" href="#publishing-prereleases" aria-hidden="true"><span class="octicon octicon-link"></span></a>Publishing prereleases</h2>

<p>As you work on a package, consider publishing it as a prerelease.
Prereleases can be useful when <em>any</em> of the following are true:</p>

<ul>
  <li>You’re actively developing the next major version of the package.</li>
  <li>You want beta testers for the next release candidate of the package.</li>
  <li>The package depends on an unstable version of the Dart or Flutter SDK.</li>
</ul>

<p>As described in <a href="https://semver.org/spec/v2.0.0-rc.1.html" class="external">semantic versioning</a>, to make a prerelease of a version
you append a suffix to the version. For example, to make a prerelease of
version <code class="language-plaintext highlighter-rouge">2.0.0</code> you might use the version <code class="language-plaintext highlighter-rouge">2.0.0-dev.1</code>. Later, when you
release version <code class="language-plaintext highlighter-rouge">2.0.0</code>, it will take precedence over all <code class="language-plaintext highlighter-rouge">2.0.0-XXX</code> prereleases.</p>

<p>Because pub prefers stable releases when available, users of a prerelease package
might need to change their dependency constraints.
For example, if a user wants to test prereleases of version 2.1, then
instead of <code class="language-plaintext highlighter-rouge">^2.0.0</code> or <code class="language-plaintext highlighter-rouge">^2.1.0</code> they might specify <code class="language-plaintext highlighter-rouge">^2.1.0-dev.1</code>.</p>

<aside class="alert alert-info" role="alert">
  <p><strong>Note:</strong>
  If a stable package in the dependency graph depends on a prerelease,
  then pub chooses that prerelease instead of a stable release.</p>
</aside>

<p>When a prerelease is published to pub.dev,
the package page displays links to both the prerelease and the stable release.
The prerelease doesn’t affect the analysis score, show up in search results,
or replace the package <code class="language-plaintext highlighter-rouge">README.md</code> and documentation.</p>

<h2 id="publishing-previews">
<a class="anchor" href="#publishing-previews" aria-hidden="true"><span class="octicon octicon-link"></span></a>Publishing previews</h2>

<p>Previews can be useful when <strong>all</strong> of the following are true:</p>

<ul>
  <li>
    <p>The next stable version of the package is complete.</p>
  </li>
  <li>
    <p>That package version depends on an API or feature in the Dart SDK that
hasn’t yet been released in a stable version of the Dart SDK.</p>
  </li>
  <li>
    <p>You know that the API or feature that the package depends on is
API-stable and won’t change before it reaches the stable SDK.</p>
  </li>
</ul>

<p>As an example, consider a new version of <code class="language-plaintext highlighter-rouge">package:args</code> that has
a finished version <code class="language-plaintext highlighter-rouge">2.0.0</code> but that
depends on a feature in Dart <code class="language-plaintext highlighter-rouge">2.12.0-259.8.beta</code>,
where Dart SDK version <code class="language-plaintext highlighter-rouge">2.12.0</code> stable hasn’t been released yet.
The pubspec might look like this:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>name: args
version: 2.0.0

environment:
sdk: '&gt;=2.12.0-259.8.beta &lt;3.0.0'
</code></pre></div></div>

<p>When this package is published to pub.dev,
it’s tagged as a preview version,
as illustrated by the following screenshot,
where the stable version is listed as
<code class="language-plaintext highlighter-rouge">1.6.0</code> and the preview version is listed as <code class="language-plaintext highlighter-rouge">2.0.0</code>.</p>

<p><img src="https://dart.dev/tools/pub/preview-version.png" alt="Illustration of a preview version" width="600px"><br></p>

<p>When Dart <code class="language-plaintext highlighter-rouge">2.12.0</code> stable is released,
pub.dev updates the package listing to display
<code class="language-plaintext highlighter-rouge">2.0.0</code> as the stable version of the package.</p>

<p>If all of the conditions at the beginning of this section are true,
then you can ignore the following warning from <code class="language-plaintext highlighter-rouge">dart pub publish</code>:</p>

<p><em>“Packages with an SDK constraint on a pre-release of the Dart SDK should
   themselves be published as a pre-release version. If this package needs Dart
   version 2.12.0-0, consider publishing the package as a pre-release
   instead.”</em></p>

<h2 id="retract">
<a class="anchor" href="#retract" aria-hidden="true"><span class="octicon octicon-link"></span></a>Retracting a package version</h2>

<p>To prevent new package consumers from adopting a recently
published version of your package, you can retract that package version
within 7 days of publication.
The retracted version can be restored again within 7 days of retraction.</p>

<p>A retracted package version isn’t deleted. It appears in the version
listing of the package on pub.dev in the <strong>Retracted versions</strong> section. Also, the
detailed view of that package version has a <strong>RETRACTED</strong> badge.</p>

<p>Before retracting a package,
consider publishing a new version instead.
Retracting a package causes churn and can have a negative impact on package users.</p>

<p>If you accidentally publish a new version with either
a <em>missing dependency constraint</em>
or a <em>dependency constraint that is too lax</em>, 
then retracting the package version might be the only solution.
Publishing a newer version of your package is
insufficient to stop the version solver from picking the old version,
which might be the only version pub can choose.
By retracting the package version that has
incorrect dependency constraints, you force users to either
upgrade other dependencies or get a dependency conflict.</p>

<p>However, if your package merely contains a minor bug,
then retraction is probably not necessary.
Publishing a newer version with the bug fixed and a
description of the fixed bug in <code class="language-plaintext highlighter-rouge">CHANGELOG.md</code>
helps users to understand what happened.
And publishing a newer version is less disruptive to package users.</p>
<strong>Version note:</strong>
  Package retraction was introduced in Dart 2.15.
  In pre-2.15 SDKs, the pub version solver ignores the retracted status.

<h3 id="how-to-use-a-retracted-package-version">
<a class="anchor" href="#how-to-use-a-retracted-package-version" aria-hidden="true"><span class="octicon octicon-link"></span></a>How to use a retracted package version</h3>

<p>If a package depends on a package version that later is retracted,
it can still use that version as long as that version is in
the dependent package’s <code class="language-plaintext highlighter-rouge">pubspec.lock</code> file.
To depend on a specific version that’s already retracted,
the dependent package must pin the version in the
<code class="language-plaintext highlighter-rouge">dependency_overrides</code> section of the <code class="language-plaintext highlighter-rouge">pubspec.yaml</code> file.</p>

<h3 id="how-to-retract-or-restore-a-package-version">
<a class="anchor" href="#how-to-retract-or-restore-a-package-version" aria-hidden="true"><span class="octicon octicon-link"></span></a>How to retract or restore a package version</h3>

<p>To retract or restore a package version,
first sign in to pub.dev using a Google Account
that’s either an uploader or a <a href="/tools/pub/verified-publishers">verified publisher</a> admin for the package.
Then go to the package’s <strong>Admin</strong> tab,
where you can retract or restore recent package versions.</p>

<h2 id="discontinue">
<a class="anchor" href="#discontinue" aria-hidden="true"><span class="octicon octicon-link"></span></a>Marking packages as discontinued</h2>

<p>Although packages always remain published, it can be useful to signal to
developers that a package is no longer being actively maintained.
For this, you can mark a package as <strong>discontinued</strong>.
A discontinued package remains published and viewable on pub.dev,
but it has a clear <strong>DISCONTINUED</strong> badge and
doesn’t appear in pub.dev search results.</p>

<p>To mark a package as discontinued, first sign in to pub.dev using a Google Account
that’s either an uploader or a <a href="/tools/pub/verified-publishers">verified publisher</a> admin for the package.
Then go to the package’s <strong>Admin</strong> tab,
where you can mark the package as discontinued.
If you change your mind, you can remove the discontinued mark at any time.</p></code></pre></div></div>

      