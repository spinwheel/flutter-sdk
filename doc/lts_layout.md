

# Long Term Layout Support

This document explains the layout context for a cross-platform Flutter app, as well as highlights the differences in comparison to how layouts work for web.

## TL;DR

Similar to how native Android handles polimorphic layouts, Flutter is also capable of providing different layouts for a same underlying presentation logic.

As denoted by the differences between *interpreted and compiled*, these layouts need to be provided during development. Web's HTML/CSS layouts are not supported and to be used would require extensive time-consuming parsing and is prone to bugs.

Instead, layouts need to be provided as a dart widget-tree for proper long term support.


## First, some references...
- [HubSpot - How HTML, CSS, and JS Work](https://blog.hubspot.com/marketing/web-design-html-css-javascript)
- [W3 schools - HTML  Layout Elements and Techniques](https://www.w3schools.com/html/html_layout.asp)
- [freecodecamp.org - Interpreted vs Compiled](https://www.freecodecamp.org/news/compiled-versus-interpreted-languages/)
- [Medium - Designing for Web vs. Apps in the Mobile Era](https://medium.com/@strv/designing-for-web-vs-apps-in-the-mobile-era-a7c2fff654df)
- [Alibaba Cloud - Breakdown between Widget and CSS](https://www.alibabacloud.com/blog/breakdown-a-detailed-comparison-between-the-flutter-widget-and-css-in-terms-of-layout-principles_596987)
- [Flutter Docs - Layouts in Flutter](https://docs.flutter.dev/development/ui/layout)
- [Flutter Docs - Building Layouts](https://docs.flutter.dev/development/ui/layout/tutorial)
- [Flutter Docs - Flutter for web devs](https://docs.flutter.dev/get-started/flutter-for/web-devs)
- [Flutter Docs - Flutter for React Native devs](https://docs.flutter.dev/get-started/flutter-for/react-native-devs)
- [Flutter Docs - Introduction to declarative UI](https://docs.flutter.dev/get-started/flutter-for/declarative)

# Layout Differences

## Flutter Layout
- [Flutter Docs - Layouts in Flutter](https://docs.flutter.dev/development/ui/layout)
- [Flutter Docs - Building Layouts](https://docs.flutter.dev/development/ui/layout/tutorial)
- [Flutter Docs - Flutter for web devs](https://docs.flutter.dev/get-started/flutter-for/web-devs)
- [Flutter Docs - Flutter for React Native devs](https://docs.flutter.dev/get-started/flutter-for/react-native-devs)
- [Flutter Docs - Introduction to declarative UI](https://docs.flutter.dev/get-started/flutter-for/declarative)

Flutter's layout differs from that of the web. Instead of using HTML and CSS, we use Dart to work with a declarative UI which creates a widget-tree.

Since we are talking about a compiled platform, these need to be fully provided at development time.

**Flutter's quickest out-of-the-box way of working with polimorphic layouts is by using a straightforward MVVM approach where we provide a ViewModel that will interact with specific widget keys, *which are unique identifiers for visual elements in flutter*.**

So long the partner provides a layout with matching widget keys, his layout will interact with our ViewModel and perform the very same operations in the very same way as our default layout and the presentation *logic* would remain in a different file, the ViewModel, unaware of layouts and unaltered.

This requires some light implementation work, but is the best approach to a long term layout support. This is the default presentation logic pattern in native Android projects, as recommended by Google.

## Web Layout

- [freecodecamp.org - Interpreted vs Compiled](https://www.freecodecamp.org/news/compiled-versus-interpreted-languages/)
- [Medium - Designing for Web vs. Apps in the Mobile Era](https://medium.com/@strv/designing-for-web-vs-apps-in-the-mobile-era-a7c2fff654df)
- [Smashing Magazine - Styling In Modern Web Apps](https://www.smashingmagazine.com/2019/06/styling-modern-web-apps/)

Web layout is heavily based on HTML and CSS. These formats are web-specific, save for a few sub-performatic hybrid solutions like React Native. These layouts are used for **interpreted apps**, so they are not out-of-the-box compatible with Flutter.

# CSS vs Widget

## CSS

CSS is a markup language used to describe styles. HTML describes the structure of web pages, while CSS describes the representation of web pages.

![head and body in css and html](assets/css_head_body.png)

Subsequent layout solutions, such as the CSS in JS frontend framework, XML description files, and Flutter, were deeply influenced by CSS even if they did not directly copy the features of CSS.

<h2>CSS Rendering</h2>

<p>The rendering process of a CSS file in a browser includes four steps: loading, parsing, querying and applying the CSS file to DOM nodes, and calculating the layout. First, the browser first the HTML file and parses it.</p>

![render steps](assets/render_steps_css_vs_flutter.png)

<p>Then, the browser generates a layout tree based on the DOM tree with ComputedStyle. The display characteristics of the nodes are different, and the types of generated LayoutObjects are also different. Then, the layout algorithm will traverse the layout tree multiple times and calculate the Rect of each node. This process is very complicated. A DOM node does not necessarily correspond to a layout node. We need to consider factors, such as display:none, pseudo element, text nodes, and shadow dom. The whole process is synchronous. After the parsed DOM node has been laid out, if the browser parses another <code>&lt;style&gt;</code> tag, it will match the CSS style in this tag, apply the new style to the parsed DOM node, and calculate the layout all over again.</p>
<p>After the layout is complete, Paint will run. The layout information is committed to the compositor thread layer by layer, and then divided into pieces and delivered to the GPU thread for drawing. I think the last two steps are relatively fast. Layout and Paint in the main thread are the most time-consuming and are mixed with the execution of JS code.</p>
<h2>Flutter Widget</h2>
<p>Unlike CSS, the Flutter Widget is well-crafted with clear classification and features. Its various properties are not coupled and do not affect each other. The design of widgets is relatively atomized, and they almost do not affect each other. Moreover, the nesting mode of widgets must meet certain requirements, so the efficiency of layout algorithms can be maintained.</p>

![widget catalog](assets/widget_catalog.png)

<p>The design of Flutter is relatively reasonable. This, to a large extent, is due to the link between Google's Flutter Development Team and Chrome. Some members of that team have been participating in the formulation of CSS specifications for many years. They are well-experienced in this field. On the whole, the new framework does not have much redundancy or bear any burdens of the past as CSS does, so it can become easier to use based on predecessors. In other words, if CSS designers could put aside burdens of the past to redesign CSS without considering backward compatibility, CSS would likely be as good as the current Flutter Widget.</p>

## Flutter Rendering

<p>Regarding the rendering process of Flutter, the official document entitled, <a href="https://flutter.dev/docs/resources/architectural-overview?spm=a2c65.11461447.0.0.12ec1c4aFsGrWt" target="_blank" data-spm-anchor-id="a2c65.11461447.0.0">How Flutter works</a>, is good learning material.</p>

![render flow](assets/render_flow.png)


# Comparisons

## Support

<p>Before we start to compare CSS and the Flutter Widget, let's take a look at which organizations support and operate them.</p>
<p>Behind CSS is the W3C, which is an industry-recognized standardization organization. CSS has been implemented by popular browsers, and browser vendors are also actively promoting the development of standards. CSS is an open technology. The big names behind it are a series of for-profit or nonprofit organizations, such as the W3C, Chrome, Safari, and Firefox. They are mutually beneficial and develop together.</p>
<p>However, only Google is behind Flutter. Flutter is open-source, but the design and implementation are led by the Google team, and very few people have the ability and opportunity to participate in the development of Flutter. PR only does small repairs. One difference between the framework and the standard lies in backward compatibility. It is common for the framework to launch an updated version with awesome optimizations and changes, but without backward compatibility.</p>
<p>From this perspective, CSS is a standardized technology with tenacious vitality, though it is bloated. The CSS code you write now can still run five years later, whereas the Flutter code you write now may or may not run five years later. If Google announced that it would not maintain Flutter anymore, the community would likely lose confidence in an instant, and Flutter would die. If Chrome announced that it would not support CSS, CSS would still be alive, but Chrome support would likely die (see IE as an example), and Firefox would be happy.</p>

## Usage

![html+css vs flutter](assets/html_css_vs_flutter.png)

<p>From the perspective of syntax, CSS is easy to learn because it is only a descriptive language and does not involve complex programming logic. The design goal of CSS is to describe "what I want the UI to be," or in other words, a result-oriented description. By contrast, widgets are implemented by Dart code. For widgets, the UI is written together with the code logic, and "How Do I Combine and Form the UI" is a process-oriented description based on the line-by-line description of code. Therefore, CSS is more intuitive, and the code written in CSS is easier to understand. There is also a small reason. Layers of nested code, such as Flutter widgets, are very troublesome to write and modify. It is too dependent on the editor and inconvenient to copy and paste.</p>
<p>CSS has been around for years, so plenty of learning materials are available. CSS specifications are detailed. Websites, such as MDN, CSS Tricks, and CodePen, provide enough resources to learn. There are also online training materials that provide clear guidance on the knowledge framework and learning path. By contrast, the Flutter Widget only has its official website, and its learning materials and community ecosystem are still far behind.</p>

## Efficiency

<p>It is easy to get started with CSS, but it is difficult to master. People without several years of development experience are unlikely to master it.</p>
<p>If you want to draw a learning curve of CSS, it must be a rapid rise at the initial stage. When it reaches a certain height, it will slow down or fall. However, when you start to learn Flutter, you must understand many concepts and change the way you think. Getting started is a slow process, but as you learn more, you will also learn faster. Writing CSS is like operating a marionette while writing a widget is like building with Lego blocks.</p>
<p>Various properties of CSS can affect each other, and the input and output are not simply corresponding. If you write width: 100px, the width may not be 100 pixels. This is like operating a puppet with hundreds of tied lines. You want to pull five lines to make the "OK" gesture, but when you pull one of the lines, you may move the whole upper body instead of just one part.</p>
<p>The Flutter Widget is more atomized and has requirements on what can be nested. This is like building with Lego blocks. Each block is small but has a clear type of bayonet. You must conform to the design, and then unleash your creativity to assemble the blocks into various shapes. Flutter compels you to combine widgets in the ideal way to avoid writing code with poor performance. However, in CSS, a property can be written with any other properties, and the standard can always give you a reasonable explanation. Therefore, the code written is very confusing, which increases the difficulty of the layout.</p>
<p>As such, the development efficiency of CSS is relatively fast in the early stages, but accumulation is difficult, and large-scale collaboration is also difficult to manage (high coupling and global scope.) By contrast, Flutter has better encapsulation, which is conducive to collaboration, and the development efficiency becomes increasingly high.</p>

## Speed

<p>Flutter has two major performance advantages in layout over CSS: one is the sub-linear layout algorithm, and the other is a more reasonable threading model.</p>
<p>The layout principle of the Flutter Widget is more efficient than CSS, which sacrifices some flexibility. When we expand the Flutter Widget in the future, we must follow these designs so the Flutter Widget can remain efficient. Complex details lie behind the simple properties of CSS. This makes the layout model increasingly complex and the rendering pipeline increasingly long. The display alone has a dozen or two dozen values, each of which has a great influence on the layout. This is convenient for developers but challenges the layout performance.</p>
<p>The threading model is also a performance bottleneck that browsers have been criticized for. The main thread is too busy, which has to deal with JS execution, HTML and CSS parsing, DOM construction, and layout calculation. By contrast, the four threads in Flutter are more balanced. The GPU thread does the same job as a browser. The code of the host platform (Android or iOS) runs in the Platform thread, the Flutter framework mainly runs in the UI thread, and the IO thread implements the loading of images, fonts, and other files from the network.</p>

## Future

<p>The W3C has defined 520 CSS styles, and 703 styles are found on the Chrome platform, including some prefixed styles. The usage of many styles is very low. The Alipay applet personnel summarized 184 different CSS styles used in the top 100 applets.</p>
<p>To sum up, 520 styles are defined in the W3C standard, and Chrome supports more than 180 private styles, but no more than 200 styles are commonly used.</p>
<p>CSS suffers many burdens of the past. I also feel that I rarely use most styles, some of which are prohibited in the best practices. After you learn 50 CSS styles, you can write 80% of layouts. For difficult styles, you can add a few more labels and write JS code to implement them. However, we cannot abandon these styles. We must support them. When a useful property is added, its compatibility with all existing properties must be clarified.</p>
<p>CSS is advancing with heavy burdens and is destined to become increasingly complex, whereas the Flutter Widget is light-packed, easy to decouple, pluggable, and combinable. If you want to abandon some widgets in the future, remove these widgets from the main package and put them in an independent plug-in. When you want to use them, import them from the plug-in. The burdens of the past have limited impact on the entire iteration process.</p>